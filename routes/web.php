<?php
//Auth
Auth::routes([
	'verify' => false,
	'register' => false
]);

Route::get('image/{filename}', 'AdminController@displayImage')->name('image.displayImage');

//Front
Route::get('/','FrontController@index')->name('front.index');
Route::get('noticias/{cat}/{name}','FrontController@single')->name('front.single');
Route::get('categoria/{cat}','FrontController@category')->name('front.category');
Route::get('categorias/{user}/{name}','FrontController@categoryUser')->name('front.category.user');
Route::get('contacto','FrontController@contact')->name('front.contact');
Route::get('busqueda','FrontController@search')->name('front.search');
Route::any('banners/{id}/{position}','FrontController@url')->name('front.url');
Route::post('save_contact','FrontController@save_contact')->name('front.save.contact');

//Admin
Route::group(['prefix' => 'admin'], function(){
	Route::get('/','AdminController@index')->name('admin.index');
	Route::post('users/upload','UserController@upload_image')->name('upload.user');
    Route::resource('users','UserController');
	Route::resource('leads','LeadController');
	Route::resource('clients','ClientController');
	Route::resource('clientspost','ClientPostController');
	Route::post('uploads/lateral','BannerController@lateral')->name('upload.lateral');
	Route::post('uploads/full','BannerController@full')->name('upload.full');
	Route::post('uploads/detail','BannerController@detail')->name('upload.detail');
	Route::post('uploads/modal','BannerController@modal')->name('upload.modal');
	Route::resource('banners','BannerController');
	Route::post('uploads/image','PostController@upload_image')->name('upload.image');
	Route::get('posts/list','PostController@getPosts')->name('posts.list');
	Route::get('posts/delete/{id}','PostController@delete')->name('posts.delete');
	Route::get('post/restore/{id}','PostController@restore')->name('post.restore');
	Route::resource('posts','PostController');
	Route::resource('systems','SystemController');
	Route::resource('categories','CategoryController');

	//Gallery
	Route::post('uploads/gallery','GalleryController@upload_gallery')->name('upload.gallery');
	Route::any('remove/gallery','GalleryController@destroy')->name('destroy.gallery');
});

Route::get('/home', 'HomeController@index')->name('home');

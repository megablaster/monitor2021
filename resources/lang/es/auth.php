<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    'remember_me' => 'Recordarme',
    'password' => 'Contraseña',
    'email' => 'Correo electrónico',
    'login' => 'Inicio de sesión',
    'forgot' => '¿Olvidaste tu contraseña?',
    'login_button' => 'Iniciar sesión',
    'reset' => 'Solicitar contraseña',
    'send_reset' => 'Solicitar contraseña',
];

<li class="nav-item" style="display: none;">
  <a href="../widgets.html" class="nav-link">
    <i class="nav-icon fas fa-th"></i>
    <p>Estadísticas</p>
  </a>
</li>
<li class="nav-item has-treeview">
  <a href="{{route('posts.index')}}" class="nav-link">
    <i class="nav-icon fas fa-tachometer-alt"></i>
    <p>
      Noticias
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{route('posts.index')}}" class="nav-link">
        <i class="far fa-file-alt nav-icon"></i>
        <p>Noticias</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{route('categories.index')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Categorías</p>
      </a>
    </li>
  </ul>
</li>
@role('admin')
<li class="nav-item has-treeview">
  <a href="{{route('users.index')}}" class="nav-link">
    <i class="nav-icon fas fa-users"></i>
    <p>
      Usuarios
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{route('users.index')}}" class="nav-link">
        <i class="far fa-user nav-icon"></i>
        <p>Usuarios</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{route('clients.index')}}" class="nav-link">
        <i class="fas fa-user-tie nav-icon"></i>
        <p>Clientes</p>
      </a>
    </li>
  </ul>
</li>
<li class="nav-item">
  <a href="{{route('banners.index')}}" class="nav-link">
    <i class="nav-icon fas fa-bullseye"></i>
    <p>Publicidad</p>
  </a>
</li>
@endrole
<li class="nav-item">
  <a href="{{route('leads.index')}}" class="nav-link">
    <i class="nav-icon far fa-envelope-open"></i>
    <p>Contacto</p>
  </a>
</li>
@role('admin')
<li class="nav-item">
  <a href="{{route('systems.index')}}" class="nav-link">
    <i class="nav-icon far fa-check-circle"></i>
    <p>Configuración</p>
  </a>
</li>
@endrole
<li class="nav-item">
  <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    <i class="nav-icon far fa-times-circle"></i>
    <p>Cerrar sesión</p>
  </a>
</li>
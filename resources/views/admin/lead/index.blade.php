@extends('layouts.admin')

@section('title','Contacto')

@section('col','col-12')

@section('subtitle','Todos las solicitudes')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Contacto</li>
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  	<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('content')
	@if (Session::has('msg-success'))
	    <div class="alert alert-success">
	        <ul>
	            <li>{!! Session::get('msg-success') !!}</li>
	        </ul>
	    </div>
	@endif
	<table id="datatable" class="table table-striped table-bordered table-sm">
      <thead class="thead-light">
          <tr class="text-center">
          	<th>#</th>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Asunto</th>
            <th>Estatus</th>
            <th>Fecha</th>
            <th>Acciones</th>
          </tr>
      </thead>
      <tbody>
      		@foreach ($leads as $lead)
	            <tr class="text-center">
	            	<td>{{$lead->id}}</td>
	                <td>{{$lead->name}}</td>
	                <td>{{$lead->email}}</td>
	                <td>{{$lead->contact}}</td>
	                <td>{!!$lead->viewed!!}</td>
	                <td>{{$lead->created_at->diffforhumans()}}</td>
	                <td>
	                	<ul>
	                		<li>
	                			<a href="{{route('leads.show',$lead->id)}}" class="btn btn-info btn-sm">Ver</a>
	                		</li>
	                		<li>
	                			<form method="POST" action="{{ route('leads.destroy',$lead->id) }}">
								    {{ csrf_field() }}
								    {{ method_field('delete') }}
								    <button type="submit" class="btn btn-danger btn-sm">Eliminar</button> 
								</form>	
	                		</li>
	                	</ul>
	                </td>
	            </tr>
            @endforeach
      </tbody>
    </table>
@endsection

@section('footer')
@endsection

@push('js')
	<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
	<script>
	 	datatables();
	</script>
@endpush
@extends('layouts.admin-empty')

@section('title','Estadísticas')

@section('subtitle','Estadísticas')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="#">Administrador</a></li>
  <li class="breadcrumb-item active">Estadísticas</li>
@endsection

@push('css')
	<style>
		.card h1 {
			font-weight: 900;
			font-size: 50px;
			margin: 0;
			padding:0;
		}

		.card p {
			margin: 0;
			font-size: 18px;
			padding:0;
		}

		a.grey {
			text-decoration: none;
			color: white;
		}

		a.grey .card:hover {
			position: relative;
			transition: all 0.3s ease;
			margin-top: -5px;
			box-shadow: 1px 10px 20px rgba(1,1,1,0.5);
		}

		a.grey .card {
			transition: all 0.3s ease;
			background-color: {{$systems->color_primary}};
		}

		a.grey .card h5 {
			margin-top: 15px;
			font-weight: 300;
		}
	</style>
@endpush

@section('content')
	
	<div class="col-lg-3">
		 <div class="card">
		      <div class="card-body text-center">
		      	@if (is_null($data['posts']->count()))
		      		<h1>-</h1>
		      	@else
		      		<h1>{{number_format($data['posts']->count())}}</h1>
		      	@endif
				<p>No. artículos</p>
		      </div>
		  </div>
	</div>

	<div class="col-lg-3">
		<div class="card">
			<div class="card-body text-center">
				@if (is_null($data['categories']->count()))
					<h1 class="text-center">-</h1>
				@else
					<h1 class="text-center">{{$data['categories']->count()}}</h1>
				@endif
				<p>No. categorías</p>
			</div>
		</div>
	</div>

	{{--<div class="col-lg-3">
		<div class="card text-center">
			<div class="card-body text-center">
				<h1 class="text-center">{{optional($data['opinions']->posts()->count())}}</h1>
				<h1 class="text-center">-</h1>
				<p>No. opiniones</p>
			</div>
		</div>
	</div>--}}

	<div class="col-lg-3">
		<div class="card">
			<div class="card-body text-center">
				@if (is_null($data['users']->count()))
					<h1 class="text-center">-</h1>
				@else
					<h1 class="text-center">{{$data['users']->count()}}</h1>
				@endif
				<p>No. usuarios</p>
			</div>
		</div>
	</div>

	<div class="col-lg-3">
		<div class="card">
			<div class="card-body text-center">
				@if (is_null($data['clients']->count()))
					<h1 class="text-center">-</h1>
				@else
					<h1 class="text-center">{{$data['clients']->count()}}</h1>
				@endif
				<p>No. clientes</p>
			</div>
		</div>
	</div>

	<div class="col-lg-3">
		<div class="card text-center">
			<div class="card-body text-center">
				@if (is_null($data['clients']->count()))
					<h1 class="text-center">-</h1>
				@else
					<h1 class="text-center">{{$data['clients']->count()}}</h1>
				@endif
				<p>No. cuentas publicitarias</p>
			</div>
		</div>
	</div>

	<div class="col-lg-3">
		<div class="card text-center">
			<div class="card-body text-center">
				@if (is_null($data['leads']->count()))
					<h1 class="text-center">-</h1>
				@else
					<h1 class="text-center">{{$data['leads']->count()}}</h1>
				@endif
				<p>Mensajes de contacto</p>
			</div>
		</div>
	</div>

	<div class="col-lg-12">
		<h4>Resumen de noticias</h4><hr>
	</div>

	<div class="col-lg-4">
		<div class="card text-center">
			<div class="card-body text-center">
				<h5>Noticia más vista del mes.</h5>
				@if ( is_null($data['moreView']) )
					<h2>-</h2>
				@else
					<h6>{{$data['moreView']->name}} - <span>{{$data['moreView']->view}} vistas.</span></h6>
				@endif
			</div>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="card text-center">
			<div class="card-body text-center">
				<h5>Noticia menos vista del mes.</h5>
				@if ( is_null($data['minusView']) )
					<h2>-</h2>
				@else
					<h6>{{$data['minusView']->name}} - <span>{{$data['minusView']->view}} vistas.</span></h6>
				@endif
			</div>
		</div>
	</div>

	<div class="col-lg-12">
		<h4>Accesos rápidos</h4><hr>
	</div>

	<div class="col-lg-3">
		<a href="{{route('posts.create')}}" class="grey">
			<div class="card text-center">
				<div class="card-body text-center">
					<i class="far fa-newspaper fa-3x"></i>
					<h5>Agregar noticia</h5>
				</div>
			</div>
		</a>
	</div>

	<div class="col-lg-3">
		<a href="{{route('categories.create')}}" class="grey">
			<div class="card text-center">
				<div class="card-body text-center">
					<i class="fas fa-bookmark fa-3x"></i>
					<h5>Agregar categoría</h5>
				</div>
			</div>
		</a>
	</div>

	@role('admin')
		<div class="col-lg-3">
			<a href="{{route('users.create')}}" class="grey">
				<div class="card text-center">
					<div class="card-body text-center">
						<i class="fas fa-users fa-3x"></i>
						<h5>Agregar usuario</h5>
					</div>
				</div>
			</a>
		</div>
	@endrole

	@role('admin')
		<div class="col-lg-3">
			<a href="{{route('clients.create')}}" class="grey">
				<div class="card text-center">
					<div class="card-body text-center">
						<i class="fas fa-user-check fa-3x"></i>
						<h5>Agregar cliente</h5>
					</div>
				</div>
			</a>
		</div>
	@endrole

	@role('admin')
		<div class="col-lg-3">
			<a href="{{route('banners.create')}}" class="grey">
				<div class="card text-center">
					<div class="card-body text-center">
						<i class="fas fa-users fa-3x"></i>
						<h5>Agregar publicidad</h5>
					</div>
				</div>
			</a>
		</div>
	@endrole

	@role('admin|editor')
		<div class="col-lg-3">
			<a href="{{route('leads.index')}}" class="grey">
				<div class="card text-center">
					<div class="card-body text-center">
						<i class="fas fa-inbox fa-3x"></i>
						<h5>Solicitudes de contacto</h5>
					</div>
				</div>
			</a>
		</div>
	@endrole
	
	@role('admin')
		<div class="col-lg-3">
			<a href="{{route('systems.index')}}" class="grey">
				<div class="card text-center">
					<div class="card-body text-center">
						<i class="fas fa-plug fa-3x"></i>
						<h5>Configuración del sistema</h5>
					</div>
				</div>
			</a>
		</div>
	@endrole
	
@endsection

@section('footer')
@endsection
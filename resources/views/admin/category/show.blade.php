@extends('layouts.admin')

@section('title',$lead->name)

@section('subtitle')
	<strong>Fecha de creación:</strong> {{$lead->created_at->format('d-m-Y')}}
@endsection

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item"><a href="{{route('leads.index')}}">Prospectos</a></li>
  <li class="breadcrumb-item active">Leads</li>
@endsection

@section('content')
	<p>
		<strong>Nombre:</strong> {{$lead->name}}<br>
		<strong>Correo:</strong> {{$lead->email}}<br>
		<strong>Teléfono:</strong> {{$lead->phone}}<br>
		<strong>Contacto:</strong> {{$lead->contact}}<br>
		<strong>Comentarios:</strong> <br>{{$lead->comments}}<br>
	</p>
@endsection

@section('footer')
	<a href="{{route('leads.index')}}" class="btn btn-info">Regresar</a>
@endsection

@push('js')
@endpush
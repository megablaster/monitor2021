@extends('layouts.admin')

@section('title','Categorías')

@section('subtitle','Todas las categorías')

@section('col','col-12')

@section('add-button')
	<li class="nav-item d-none d-sm-inline-block">
	    <a href="{{route('categories.create')}}" class="btn btn-success" style="color:#fff;">Agregar</a>
	</li>
@endsection

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Categorías</li>
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  	<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('content')
	@if (Session::has('msg-success'))
	    <div class="alert alert-success">
	        <ul>
	            <li>{!! Session::get('msg-success') !!}</li>
	        </ul>
	    </div>
	@endif
	@if (Session::has('msg-error'))
	    <div class="alert alert-danger">
	        <ul>
	            <li>{!! Session::get('msg-error') !!}</li>
	        </ul>
	    </div>
	@endif
	<table id="datatable" class="table table-striped table-bordered table-sm">
      <thead class="thead-light">
          <tr class="text-center">
            <th>Nombre</th>
            <th>Noticias</th>
            <th>Estatus</th>
            <th>Fecha</th>
            <th>Acciones</th>
          </tr>
      </thead>
      <tbody>
      		@foreach ($categories as $category)
	            <tr class="text-center">
	                <td>{{$category->name}}</td>
	                <td>{{$category->posts->count()}}</td>
	                <td>{{$category->statu}}</td>
	                <td>{{$category->created_at}}</td>
	                <td>
	                	<ul>
	                		<li>
	                			<a href="{{route('categories.edit',$category->id)}}" class="btn btn-info btn-xs">Editar</a>
	                		</li>
	                		@role('admin')
	                		<li>
	                			<form method="post" action="{{ route('categories.destroy',$category->id) }}">
								    @csrf
								    {{ method_field('delete') }}
								    <button type="submit" class="btn btn-danger btn-xs">Eliminar</button> 
								</form>	
	                		</li>
	                		@endrole
	                	</ul>
	                </td>
	            </tr>
            @endforeach
      </tbody>
    </table>
@endsection

@section('footer')
@endsection

@push('js')
	<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
	<script>
		datatables();
	</script>
@endpush
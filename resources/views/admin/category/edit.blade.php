@extends('layouts.admin')

@section('title', $category->name)

@section('add-button')
	<a href="{{route('categories.index')}}" class="btn btn-danger" style="margin-right: 5px;">Cancelar</a>
	<a href="#" id="save" class="btn btn-success">Guardar cambios</a>
@endsection

@section('subtitle')
@endsection

@section('col','col-12')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Categorías</li>
@endsection

@section('content')
	
	<form method="post" id="form-global" action="{{route('categories.update',$category->id)}}"  enctype="multipart/form-data">
		@if ($errors->any())
		     @foreach ($errors->all() as $error)
		         <div>{{$error}}</div>
		     @endforeach
		 @endif
		@csrf
		{{ method_field('PUT') }}
		<div class="form-group">
			<label>Estatus</label><br>
			<input type="checkbox" name="status" {{$category->status == 'on' ? 'checked':''}} data-bootstrap-switch data-on-color="success">
		</div>
		<div class="form-group">
			<label>Mostrar en inicio</label><br>
			<input type="checkbox" name="featured" {{$category->featured == 'on' ? 'checked':''}} data-bootstrap-switch data-on-color="success">
		</div>
		<div class="form-group">
			<label>Mostrar autores</label><br>
			<input type="checkbox" name="author" {{$category->author == 'on' ? 'checked':''}} data-bootstrap-switch data-on-color="success">
		</div>
		<div class="form-group">
			<label>Nombre:</label>
			<input type="text" name="name" class="form-control" value="{{$category->name}}" required>
		</div>
		<div class="form-group">
			<label>Imagen:</label>
			<input type="file" name="img" class="form-control"><br>
			<img src="{{$category->img}}" style="width: 300px;">
		</div>
		<button type="submit" class="btn btn-success btn-hidden">Guardar</button>
	</form>

@endsection

@section('footer')
@endsection

@push('js')
	<script src="{{asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
	<script>
		switch_init();
	</script>
@endpush
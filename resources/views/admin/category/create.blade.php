@extends('layouts.admin')

@section('title','Agregar categoría')

@section('add-button')
	<a href="{{route('categories.index')}}" class="btn btn-info">Regresar</a>
@endsection

@section('subtitle')
@endsection

@section('col','col-5')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Categorías</li>
@endsection

@section('content')
	
	<form method="post" action="{{route('categories.store')}}"  enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label>Nombre:</label>
			<input type="text" name="name" class="form-control" required>
		</div>
		<button type="submit" class="btn btn-success btn-block">Agregar categoría</button>
	</form>

@endsection

@section('footer')
@endsection

@push('js')
@endpush
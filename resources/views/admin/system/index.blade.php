@extends('layouts.admin')

@section('title','Noticias')

@section('subtitle','Todas las noticias')

@section('col','col-12')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Noticias</li>
@endsection

@section('add-button')
	<li class="nav-item d-none d-sm-inline-block">
	    <a href="{{route('posts.create')}}" class="btn btn-success" style="color:#fff;">Agregar</a>
	</li>
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  	<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('content')
	@if (Session::has('msg-success'))
	    <div class="alert alert-success">
	        <ul>
	            <li>{!! Session::get('msg-success') !!}</li>
	        </ul>
	    </div>
	@endif
	<table id="datatable" class="table table-striped table-bordered table-sm">
      <thead class="thead-light">
          <tr class="text-center">
          	<th>#</th>
            <th>Nombre</th>
            <th>Estatus</th>
            <th>Fecha</th>
            <th>Acciones</th>
          </tr>
      </thead>
      <tbody>
      		@foreach ($posts as $post)
	            <tr class="text-center">
	            	<td>{{$post->id}}</td>
	                <td>{{$post->name}}</td>
	                <td>{!!$post->viewed!!}</td>
	                <td>{{$post->created_at->diffforhumans()}}</td>
	                <td>
	                	<ul>
	                		<li>
	                			<a href="{{route('posts.show',$post->id)}}" class="btn btn-info btn-sm">Ver</a>
	                		</li>
	                		<li>
	                			<a href="{{route('posts.edit',$post->id)}}" class="btn btn-success btn-sm">Editar</a>
	                		</li>
	                		<li>
	                			<form method="POST" action="{{ route('posts.destroy',$post->id) }}">
								    {{ csrf_field() }}
								    {{ method_field('delete') }}
								    <button type="submit" class="btn btn-danger btn-sm">Eliminar</button> 
								</form>	
	                		</li>
	                	</ul>
	                </td>
	            </tr>
            @endforeach
      </tbody>
    </table>
@endsection

@section('footer')
@endsection

@push('js')
	<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
	<script>
	  $(function () {
	    $('#datatable').DataTable({
	      "paging": true,
	      "lengthChange": true,
	      "searching": true,
	      "ordering": true,
	      "info": true,
	      "autoWidth": false,
	      "responsive": true,
	    });
	  });
	</script>
@endpush
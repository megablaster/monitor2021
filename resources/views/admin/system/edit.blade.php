@extends('layouts.admin')

@section('title','Configuración')

@section('add-button')
	<a href="{{route('admin.index')}}" class="btn btn-danger" style="margin-right: 5px;">Cancelar</a>
	<a href="#" id="save" class="btn btn-success">Guardar cambios</a>
@endsection

@section('subtitle')
@endsection

@section('col','col-12')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Configuración</li>
@endsection

@push('css')
@endpush

@section('content')
	
	<form method="post" id="form-global" action="{{route('systems.update',$system->id)}}"  enctype="multipart/form-data">
		@if ($errors->any())
	     	@foreach ($errors->all() as $error)
		     	<div class="alert alert-danger">
			        <ul>
			            <li>{{$error}}</li>
			        </ul>
			    </div>
	     	@endforeach
		@endif
		@if (Session::has('msg-success'))
		    <div class="alert alert-success">
		        <ul>
		            <li>{!! Session::get('msg-success') !!}</li>
		        </ul>
		    </div>
		@endif
		@csrf
		{{ method_field('PUT') }}
		<div class="form-group">
			<label>Nombre del negocio:</label>
			<input type="text" name="name_company" class="form-control" value="{{$system->name_company}}">
		</div>
		<div class="form-group">
			<label>Teléfono:</label>
			<input type="text" name="phone" class="form-control" value="{{$system->phone}}">
		</div>
		<div class="form-group">
			<label>Correo:</label>
			<input type="text" name="email" class="form-control" value="{{$system->email}}">
		</div>
		<div class="row">
			<div class="col-xl-3">
				<div class="form-group">
					<label>Facebook:</label>
					<input type="text" name="facebook" class="form-control" value="{{$system->facebook}}">
				</div>
			</div>
			<div class="col-xl-3">
				<div class="form-group">
					<label>Twitter:</label>
					<input type="text" name="twitter" class="form-control" value="{{$system->twitter}}">
				</div>
			</div>
			<div class="col-xl-3">
				<div class="form-group">
					<label>Linkedin:</label>
					<input type="text" name="linkedin" class="form-control" value="{{$system->linkedin}}">
				</div>
			</div>
			<div class="col-xl-3">
				<div class="form-group">
					<label>Instagram:</label>
					<input type="text" name="instagram" class="form-control" value="{{$system->instagram}}">
				</div>
			</div>
			<div class="col-xl-3">
				<div class="form-group">
					<label>Usuario twitter:</label>
					<input type="text" name="twitter_name" class="form-control" value="{{$system->twitter_name}}">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-6">
				<div class="form-group">
					<label>WhatsApp:</label>
					<input type="text" name="whatsapp" class="form-control" value="{{$system->whatsapp}}">
				</div>
			</div>
			<div class="col-xl-6">
				<div class="form-group">
					<label>Sitio web:</label>
					<input type="url" name="website" class="form-control" value="{{$system->website}}">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label>Dirección:</label>
			<textarea class="form-control" name="direction">{{$system->direction}}</textarea>
		</div>
		<div class="row">
			<div class="col-xl-12">
				<h4><strong>Personalización</strong></h4><hr>
			</div>
			<div class="col-xl-6">
				<div class="form-group">
					<label>Logotipo:</label>
					<input type="file" name="logotipo" class="form-control"><hr>
					<img src="{{$system->logo}}" class="img-fluid" style="max-width: 300px;">
				</div>
			</div>
			<div class="col-xl-6">
				<div class="form-group">
					<label>Favicon:</label>
					<input type="file" name="favicon" class="form-control"><hr>
					<img src="{{$system->favicon}}" class="img-fluid" style="max-width: 300px;">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-4">
				<div class="form-group">
					<label>Color general:</label>
					<input type="text" name="color_primary" class="form-control" value="{{$system->color_primary}}">
				</div>
			</div>
			<div class="col-xl-4">
				<div class="form-group">
					<label>Color secundario:</label>
					<input type="text" name="color_secondary" class="form-control" value="{{$system->color_secondary}}">
				</div>
			</div>
			<div class="col-xl-4">
				<div class="form-group">
					<label>Color de fondo:</label>
					<input type="text" name="color_bar" class="form-control" value="{{$system->color_bar}}">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label>Google analitycs:</label>
			<textarea class="form-control" name="analytics" rows="7" cols="50">{{$system->analytics}}</textarea>
		</div>
		<button type="submit" class="btn btn-success btn-hidden">Guardar</button>
	</form>

@endsection

@section('footer')
@endsection

@push('js')
@endpush
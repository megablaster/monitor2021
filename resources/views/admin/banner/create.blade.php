@extends('layouts.admin')

@section('title','Agregar cuenta publicitaria')

@section('add-button')
	<a href="{{route('posts.index')}}" class="btn btn-info">Regresar</a>
@endsection

@section('subtitle')
@endsection

@section('col','col-12')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Banner</li>
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('assets/plugins/summernote/summernote-bs4.css')}}">
@endpush

@section('content')
	
	<form method="post" action="{{route('banners.store')}}">
		@csrf
		<div class="col-xl-1 col-lg-2">
			<div class="form-group">
				<label>Estatus:</label><br>
				<input type="checkbox" name="status" data-bootstrap-switch data-on-color="success">
			</div>
		</div>
		<div class="form-group">
			<label>Nombre:</label>
			<input type="text" name="name" class="form-control" required>
		</div>
		<div class="form-group">
			<label>Cliente:</label>
			<select class="form-control" name="user_id">
				@foreach ($data['clients'] as $client)
					<option value="{{$client->id}}">{{$client->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Categoría:</label>
			<select class="form-control" name="category_id">
				@foreach ($data['categories'] as $category)
					<option value="{{$category->id}}">{{$category->name}}</option>
				@endforeach
			</select>
		</div>
		<button type="submit" class="btn btn-success">Cotinuar</button>
	</form>

@endsection

@section('footer')
@endsection

@push('js')
	<script src="{{asset('assets/plugins/ckeditor/build/ckeditor.js')}}"></script>
	<script src="{{asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
	<script src="{{asset('assets/plugins/summernote/summernote-bs4.min.js')}}"></script>
	<script>
		switch_init();
	</script>
@endpush
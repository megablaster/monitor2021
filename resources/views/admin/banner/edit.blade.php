@extends('layouts.admin-empty')

@section('title','Editar publicidad')

@section('add-button')
	<a href="{{route('banners.index')}}" class="btn btn-danger" style="margin-right: 5px;">Regresar</a>
	<a href="#" id="save" class="btn btn-success">Guardar cambios</a>
@endsection

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Publicidad</li>
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('assets/plugins/owl/assets/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/plugins/owl/assets/owl.theme.default.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/plugins/summernote/summernote-bs4.css')}}">
@endpush

@section('content')

	<div class="col-md-12">
	    <div class="card">
	      <div class="card-header">
	        <h3 class="card-title">@yield('subtitle')</h3>
	      </div>
	      <div class="card-body">
	      	@if (Session::has('msg-success'))
			    <div class="alert alert-success">
			        <ul>
			            <li>{!! Session::get('msg-success') !!}</li>
			        </ul>
			    </div>
			@endif
			<form method="post" id="form-global" action="{{route('banners.update',$data['banner']->id)}}"  enctype="multipart/form-data">
				@if ($errors->any())
					<div class="alert alert-danger">
				        <ul>
				           @foreach ($errors->all() as $error)
					           <div>{{$error}}</div>
					     	@endforeach
				        </ul>
				    </div>
				@endif
				@csrf
				{{ method_field('PUT') }}
				<div class="row">
					<div class="col-xl-1 col-lg-2">
						<div class="form-group">
							<label>Estatus:</label><br>
							<input type="checkbox" name="status" {{$data['banner']->status == 'on' ? 'checked':''}} data-bootstrap-switch data-on-color="success">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>Nombre:</label>
							<input type="text" name="name" class="form-control" value="{{$data['banner']->name}}">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Cliente:</label>
							<input type="text" name="name" class="form-control" value="{{$data['banner']->client->name}}" disabled>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Categoría:</label>
							<select class="form-control" name="category_id">
								@foreach ($data['categories'] as $category)
									<option value="{{$category->id}}"  {{$data['banner']->category_id == $category->id ? 'selected':''}}>{{$category->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Url:</label>
							<input type="text" class="form-control" name="url" value="{{$data['banner']->url}}">
						</div>
					</div>
				</div>
				<div class="row">

					@if($data['banner']->category_id != 19)
						<div class="col-xl-3 col-lg-3">
							<div class="form-group">
								<label>Flotante categoría:</label><br>
								<input type="checkbox" name="status_modal" {{$data['banner']->status_modal == 'on' ? 'checked':''}} data-bootstrap-switch data-on-color="success">
							</div>
						</div>
					@endif
					@if($data['banner']->category_id != 19)
						<div class="col-xl-3 col-lg-3">
							<div class="form-group">
								<label>Derecha (Portada):</label><br>
								<input type="checkbox" name="status_home_left" {{$data['banner']->status_home_left == 'on' ? 'checked':''}} data-bootstrap-switch data-on-color="success">
							</div>
						</div>
					@endif
					<div class="col-xl-3 col-lg-3">
						<div class="form-group">
							<label>Horizontal (Portada y Categoría):</label><br>
							<input type="checkbox" name="status_home_full" {{$data['banner']->status_home_full == 'on' ? 'checked':''}} data-bootstrap-switch data-on-color="success">
						</div>
					</div>
					@if($data['banner']->category_id != 19)
						<div class="col-xl-3 col-lg-3">
							<div class="form-group">
								<label>Detalle artículo:</label><br>
								<input type="checkbox" name="status_article" {{$data['banner']->status_article == 'on' ? 'checked':''}} data-bootstrap-switch data-on-color="success">
							</div>
						</div>
					@endif
				</div>
				<button type="submit" class="btn btn-success btn-hidden">Guardar</button>
			</form>
	      </div>
	    </div>
	    <!-- /.card -->
	</div>

	@if($data['banner']->category_id != 19)
		<div class="col-md-4">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Flotante categoría (600x400)</h3>
				</div>
				<div class="card-body">
					<form action="{{route('upload.modal')}}" enctype="multipart/form-data" method="post">
						@csrf
						<div class="form-group">
							<input type="hidden" name="id" value="{{$data['banner']->id}}">
							<input type="file" name="img" class="form-control"><hr>
							<img src="{{$data['banner']->img_modal}}" class="img-fluid" style="max-width: 200px;">
							<br>
						</div>
						<hr>
						<button type="submit" class="btn btn-primary">Subir</button>
					</form>
				</div>
			</div>
		</div>
	@endif

	@if($data['banner']->category_id != 19)
	<div class="col-md-4">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Portada derecha (600x400)</h3>
			</div>
			<div class="card-body">
				<form action="{{route('upload.lateral')}}" enctype="multipart/form-data" method="post">
					@csrf
					<div class="form-group">
						<input type="hidden" name="id" value="{{$data['banner']->id}}">
						<input type="file" name="img" class="form-control"><hr>
						<img src="{{$data['banner']->img_home_left}}" class="img-fluid" style="max-width: 200px;">
						<br>
					</div>
					<hr>
					<button type="submit" class="btn btn-primary">Subir</button>
				</form>
			</div>
		</div>
	</div>
	@endif

	<div class="col-md-4">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Portada horizontal (1100x340)</h3>
			</div>
			<div class="card-body">
				<form action="{{route('upload.full')}}" enctype="multipart/form-data" method="post">
					@csrf
					<div class="form-group">
						<input type="hidden" name="id" value="{{$data['banner']->id}}">
						<input type="file" name="img" class="form-control"><hr>
						<img src="{{$data['banner']->img_home_full}}" class="img-fluid" style="max-width: 200px;">
						<br>
					</div>
					<hr>
					<button type="submit" class="btn btn-primary">Subir</button>
				</form>
			</div>
		</div>
	</div>

	@if($data['banner']->category_id != 19)
		<div class="col-md-4">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Detalle artículo (600x400)</h3>
				</div>
				<div class="card-body">
					<form action="{{route('upload.detail')}}" enctype="multipart/form-data" method="post">
						@csrf
						<div class="form-group">
							<input type="hidden" name="id" value="{{$data['banner']->id}}">
							<input type="file" name="img" class="form-control"><hr>
							<img src="{{$data['banner']->img_article}}" class="img-fluid" style="max-width: 200px;">
							<br>
						</div>
						<hr>
						<button type="submit" class="btn btn-primary">Subir</button>
					</form>
				</div>
			</div>
		</div>
	@endif
	
@endsection

@section('footer')
@endsection

@push('js')
	<script src="{{asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
	<script>
		switch_init();
	</script>
@endpush
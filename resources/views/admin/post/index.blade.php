@extends('layouts.admin')

@section('title','Noticias')

@section('subtitle','Todas las noticias')

@section('col','col-12')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Noticias</li>
@endsection

@section('add-button')
	<li class="nav-item d-none d-sm-inline-block">
	    <a href="{{route('posts.create')}}" class="btn btn-success" style="color:#fff;">Agregar</a>
	</li>
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  	<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('content')
	@if (Session::has('msg-success'))
	    <div class="alert alert-success">
	        <ul>
	            <li>{!! Session::get('msg-success') !!}</li>
	        </ul>
	    </div>
	@endif
	<table id="datatable" class="table table-striped table-bordered table-sm yajra-datatable">
      <thead class="thead-light">
          <tr class="text-center">
          	<th>No</th>
            <th>Nombre</th>
            <th>Estatus</th>
            <th>Categoría</th>
            <th>Visitas</th>
            <th>Fecha</th>
            <th>Acciones</th>
          </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
@endsection

@section('footer')
@endsection

@push('js')
	<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript">
	  $(function () {
	    var table = $('.yajra-datatable').DataTable({
	        processing: true,
	        serverSide: true,
	        pageLength: 50,
	        order: [[ 0, "desc" ]],
	        ajax: "{{ route('posts.list') }}",
	        columns: [
	            {searchable: true, orderable: true, data: 'id', name: 'id'},
	            {data: 'cuttitle', name: 'cuttitle'},
	            {data: 'status', name: 'status'},
	            {data: 'category', name: 'category'},
	            {data: 'view', name: 'view'},
	            {data: 'date', name: 'date'},
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: true, 
	                searchable: true
	            },
	        ]
	    });
	    
	  });
	</script>
@endpush
@extends('layouts.admin')

@section('title','Agregar noticia')

@section('add-button')
	<a href="{{route('posts.index')}}" class="btn btn-info">Regresar</a>
@endsection

@section('subtitle')
@endsection

@section('col','col-12')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Noticias</li>
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('assets/plugins/summernote/summernote-bs4.css')}}">
@endpush

@section('content')
	
	<form method="post" action="{{route('posts.store')}}"  enctype="multipart/form-data">
		@csrf
		<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
		<div class="form-group">
			<label>Nombre:</label>
			<input type="text" name="name" class="form-control" required>
		</div>
		<div class="form-group">
			<label>Categoría:</label>
			<select class="form-control" name="category_id">
				@foreach ($data['categories'] as $category)
					<option value="{{$category->id}}">{{$category->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Descripción:</label>
			<textarea class="editor" name="description"></textarea>
		</div>
		<button type="submit" class="btn btn-success">Continuar</button>
	</form>

@endsection

@section('footer')
@endsection

@push('js')
	<script src="{{asset('assets/plugins/ckeditor/build/ckeditor.js')}}"></script>
	<script src="{{asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
	<script src="{{asset('assets/plugins/summernote/summernote-bs4.min.js')}}"></script>
	<script>
		switch_init();
		summernote_init();
	</script>
	<script>ClassicEditor
		.create( document.querySelector( '.editor' ), {
			
			toolbar: {
				items: [
					'heading',
					'|',
					'fontSize',
					'fontColor',
					'horizontalLine',
					'bold',
					'italic',
					'link',
					'removeFormat',
					'bulletedList',
					'numberedList',
					'|',
					'indent',
					'outdent',
					'|',
					'imageUpload',
					'imageInsert',
					'insertTable',
					'mediaEmbed',
					'undo',
					'redo'
				]
			},
			language: 'es',
			image: {
				toolbar: [
					'imageTextAlternative',
					'imageStyle:full',
					'imageStyle:side'
				]
			},
			table: {
				contentToolbar: [
					'tableColumn',
					'tableRow',
					'mergeTableCells'
				]
			},
			licenseKey: '',
			
		} )
		.then( editor => {
			window.editor = editor;
		})
		.catch( error => {
			console.error( 'Oops, something went wrong!' );
			console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
			console.warn( 'Build id: refjhhoyjdf1-pc5cy97iyk66' );
			console.error( error );
		});
	</script>
@endpush
@extends('layouts.admin')

@section('title',$post->name)

@section('col','col-12')

@section('subtitle')
	<strong>Fecha de creación:</strong> {{$post->created_at->format('d-m-Y')}}
@endsection

@section('breadcrumb')
@endsection

@section('content')
	<p>
		<strong>Nombre:</strong> {{$post->name}}<br>
		<strong>Estatus:</strong> {!!$post->statu!!}<br>
		<strong>Categoría:</strong> {{$post->category->name}}<br>
		<strong>Descripción:</strong> <br>{!!$post->description!!}
	</p>
@endsection

@section('footer')
	<a href="{{route('posts.index')}}" class="btn btn-info">Regresar</a>
@endsection

@push('js')
@endpush
@extends('layouts.admin-empty')

@section('title','Editar noticia')

@section('add-button')
	<a href="{{route('posts.index')}}" class="btn btn-danger" style="margin-right: 5px;">Cancelar</a>
	<a href="#" id="save" class="btn btn-success">Guardar cambios</a>
@endsection

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Noticias</li>
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('assets/plugins/owl/assets/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/plugins/owl/assets/owl.theme.default.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/plugins/summernote/summernote-bs4.css')}}">
@endpush

@section('content')

	<div class="col-md-12">
	    <!-- Default box -->
	    <div class="card">
	      <div class="card-header">
	        <h3 class="card-title">@yield('subtitle')</h3>
	      </div>
	      <div class="card-body">
			<form method="post" id="form-global" action="{{route('posts.update',$data['post']->id)}}"  enctype="multipart/form-data">
				@if ($errors->any())
					<div class="alert alert-danger">
				        <ul>
				           @foreach ($errors->all() as $error)
					           <div>{{$error}}</div>
					     	@endforeach
				        </ul>
				    </div>
				@endif
				@csrf
				{{ method_field('PUT') }}
				<div class="row">
					<div class="col-xl-1 col-lg-2">
						<div class="form-group">
							<label>Estatus:</label><br>
							<input type="checkbox" name="status" {{$data['post']->status == 'on' ? 'checked':''}} data-bootstrap-switch data-on-color="success">
						</div>
					</div>
					<div class="col-xl-1 col-lg-2">
						<div class="form-group">
							<label>Destacar</label><br>
							<input type="checkbox" name="featured" {{$data['post']->featured == 'on' ? 'checked':''}} data-bootstrap-switch data-on-color="info">
						</div>
					</div>
					<div class="col-xl-1 col-lg-2">
						<div class="form-group">
							<label>Anónimo</label><br>
							<input type="checkbox" name="anonymous" {{$data['post']->anonymous == 'on' ? 'checked':''}} data-bootstrap-switch data-on-color="danger">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>Nombre:</label>
							<input type="text" name="name" class="form-control" value="{{$data['post']->name}}">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Fecha de publicación:</label>
							<input type="date" name="created_at" class="form-control" value="{{ $data['post']->created_at->format('Y-m-d') }}">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Categoría:</label>
							<select class="form-control" name="category_id">
								@foreach ($data['categories'] as $category)
									<option value="{{$category->id}}"  {{$data['post']->category_id == $category->id ? 'selected':''}}>{{$category->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Autor:</label>
							<select class="form-control" name="autor_id">
								@foreach ($data['autors'] as $autor)
									<option value="{{$autor->id}}"  {{$data['post']->user_id == $autor->id ? 'selected':''}}>{{$autor->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Publicidad:</label>
							<select class="form-control" name="banner_id">
									<option value="">-- Selecciona la publicidad --</option>
								@foreach ($data['banners'] as $banner)
									<option value="{{$banner->id}}" {{$data['post']->banner_id == $banner->id ? 'selected':''}}>{{$banner->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Asignar cliente a noticia:</label>
							<select class="form-control" name="client_id">
									<option value="">-- Asignar a un cliente --</option>
								@foreach ($data['clients'] as $client)
									<option value="{{$client->id}}" {{ @$data['clientPost']->client_id == $client->id ? 'selected':''}}>{{$client->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Descripción:</label>
					<div id="toolbar-container"></div>
					<textarea class="editor" name="description">{!!$data['post']->description!!}</textarea>
				</div>
				<button type="submit" class="btn btn-success btn-hidden">Guardar noticia</button>
			</form>
	      </div>
	    </div>
	    <!-- /.card -->
	</div>

	<div class="col-md-4">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Portada</h3>
			</div>
			<div class="card-body">
				<form action="{{route('upload.image')}}" enctype="multipart/form-data" method="post">
					@csrf
					<div class="form-group">
						<label>Portada:</label>
						<input type="hidden" name="id" value="{{$data['post']->id}}">
						<input type="file" name="img" class="form-control"><hr>
						<img src="{{$data['post']->img}}" class="img-fluid">
						<br>
					</div>
					<hr>
					<button type="submit" class="btn btn-primary">Subir portada</button>
				</form>
			</div>
		</div>
	</div>

	<div class="col-md-8">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Galería</h3>
			</div>
			<div class="card-body">

				<form action="{{route('upload.gallery')}}" enctype="multipart/form-data" method="post">
					@csrf
					<input type="hidden" name="id" value="{{$data['post']->id}}">
					<div class="form-group">
						<input type="file" name="gallery[]" class="form-control" multiple><hr>
					</div>
					<button type="submit" class="btn btn-primary">Subir galería</button>
				</form><hr>

				<div class="row" id="galleries-post">
					<div class="col-md-12">
						<div class="owl-carousel owl-theme" id="carousel">

							@foreach ($data['galleries'] as $gal)
								<div class="item">
									<div class="img" style="background-image: url('{{$gal->img}}');">
										<form method="POST" action="{{ route('destroy.gallery') }}">
										    @csrf
										    <input type="hidden" name="id" value="{{$gal->id}}">
										    <button type="submit" class="btn btn-danger delete btn-xs">Eliminar</button> 
										</form>	
									</div>
								</div>
							@endforeach

						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
	
@endsection

@section('footer')
@endsection

@push('js')
	<script src="{{asset('assets/plugins/ckeditor/build/ckeditor.js')}}"></script>
	<script src="{{asset('assets/plugins/owl/owl.carousel.min.js')}}"></script>
	<script src="{{asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
	<script>
		switch_init();
		owl();
	</script>
	<script>ClassicEditor
		.create( document.querySelector( '.editor' ), {
			
			toolbar: {
				items: [
					'heading',
					'|',
					'fontSize',
					'fontColor',
					'horizontalLine',
					'bold',
					'italic',
					'link',
					'removeFormat',
					'bulletedList',
					'numberedList',
					'|',
					'indent',
					'outdent',
					'|',
					'imageUpload',
					'imageInsert',
					'insertTable',
					'mediaEmbed',
					'undo',
					'redo'
				]
			},
			language: 'es',
			image: {
				toolbar: [
					'imageTextAlternative',
					'imageStyle:full',
					'imageStyle:side'
				]
			},
			table: {
				contentToolbar: [
					'tableColumn',
					'tableRow',
					'mergeTableCells'
				]
			},
			licenseKey: '',
			
		} )
		.then( editor => {
			window.editor = editor;
		})
		.catch( error => {
			console.error( 'Oops, something went wrong!' );
			console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
			console.warn( 'Build id: refjhhoyjdf1-pc5cy97iyk66' );
			console.error( error );
		});
	</script>
@endpush
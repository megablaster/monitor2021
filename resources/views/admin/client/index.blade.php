@extends('layouts.admin')

@section('title','Clientes')

@section('subtitle','Todos los usuarios')

@section('col','col-12')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Clientes</li>
@endsection

@section('add-button')
	<li class="nav-item d-none d-sm-inline-block">
	    <a href="{{route('users.create')}}" class="btn btn-success" style="color:#fff;">Agregar</a>
	</li>
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  	<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('content')
	@if (Session::has('msg-success'))
	    <div class="alert alert-success">
	        <ul>
	            <li>{!! Session::get('msg-success') !!}</li>
	        </ul>
	    </div>
	@endif
	<table id="datatable" class="table table-striped table-bordered table-sm">
      <thead class="thead-light">
          <tr class="text-center">
          	<th>#</th>
            <th>Nombre</th>
            <th>Fecha de registro</th>
            <th>Rol</th>
            <th>Acciones</th>
          </tr>
      </thead>
      <tbody>
      		@foreach ($data['users'] as $user)
	            <tr class="text-center">
	            	<td>{{$user->id}}</td>
	                <td>{{$user->name}}</td>
	                <td>{{$user->date}}</td>
	                <td>
            			<span class="badge badge-warning">Cliente</span>
	                </td>
	                <td>
	                	<ul>
	                		<li>
	                			<a href="{{route('clients.edit',$user->id)}}" class="btn btn-success btn-xs">Editar</a>
	                		</li>
	                		<li>
	                			<form method="POST" action="{{ route('clients.destroy',$user->id) }}">
								    @csrf
								    {{ method_field('delete') }}
								    <button type="submit" class="btn btn-danger btn-xs">Eliminar</button> 
								</form>	
	                		</li>
	                	</ul>
	                </td>
	            </tr>
            @endforeach
      </tbody>
    </table>
@endsection

@section('footer')
@endsection

@push('js')
	<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
	<script>
	  datatables();
	</script>
@endpush
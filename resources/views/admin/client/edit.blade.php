@extends('layouts.admin')

@section('title','Editar cliente')

@section('add-button')
	<a href="{{route('clients.index')}}" class="btn btn-danger" style="margin-right: 5px;">Cancelar</a>
	<a href="#" id="save" class="btn btn-success">Guardar cambios</a>
@endsection

@section('subtitle')
@endsection

@section('col','col-12')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Clientes</li>
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  	<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">

  	<style>
  		.banners ul {
  			display: inline-block;
  			margin:0 auto;
  			text-align: center;
  		}

  		.banners ul li {
  			list-style-type: none;
  			display: inline-block;
  		}
  	</style>
@endpush

@section('content')
	
	@if (Session::has('msg-success'))
	    <div class="alert alert-success">
	        <ul>
	            <li>{!! Session::get('msg-success') !!}</li>
	        </ul>
	    </div>
	@endif
	<form method="post" id="form-global" action="{{route('clients.update',$user->id)}}">
		@if ($errors->any())
	     	@foreach ($errors->all() as $error)
	           <div>{{$error}}</div>
	     	@endforeach
		@endif
		@csrf
		{{ method_field('PUT') }}
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<label>Nombre:</label>
					<input type="text" name="name" class="form-control" value="{{$user->name}}" required>
				</div>`
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label>Correo electrónico:</label>
					<input type="text" name="email" class="form-control" value="{{$user->email}}" required>
				</div>
			</div>
			@role('admin')
				<div class="col-lg-3">
					<div class="form-group">
						<label>Roles:</label>
						<select class="form-control" name="rol" required>
							<option value="admin">Administrador</option>
							<option value="editor">Editor</option>
							<option value="columnista">Columnista</option>
							<option value="cliente" selected>Cliente</option>
						</select>
					</div>
				</div>
			@endrole
			<div class="col-lg-3">
				<div class="form-group">
					<label>RFC:</label>
					<input type="text" name="rfc" class="form-control" value="{{$user->rfc}}">
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label>Teléfono:</label>
					<input type="tel" name="phone" class="form-control" value="{{$user->phone}}">
				</div>
			</div>
			<div class="col-lg-12">
				<div class="form-group">
					<label>Dirección:</label>
					<textarea class="form-control" name="direction">{{$user->direction}}</textarea>
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-success btn-hidden">Guardar</button>
	</form>

	<h2>Noticias</h2>

	<table class="datatable table table-striped table-sm">
      <thead class="thead-light">
          <tr class="text-center">
            <th>Nombre</th>
            <th>Fecha de creación</th>
            <th>Visitas</th>
            <th>Acciones</th>
          </tr>
      </thead>
      <tbody>
      		@foreach ($posts as $post)
	            <tr class="text-center">
	                <td>{{$post->post->name}}</td>
	                <td>{{$post->post->date}}</td>
	                <td>
            			<span class="badge badge-warning">{{$post->post->view}}</span>
	                </td>
	                <td>
            			<form method="POST" action="{{ route('clientspost.destroy',$post->id) }}">
						    @csrf
						    <input type="hidden" name="id" value="{{$post->id}}">
						    {{ method_field('delete') }}
						    <button type="submit" class="btn btn-danger btn-xs">Eliminar</button> 
						</form>
	                </td>
	            </tr>
            @endforeach
      </tbody>
    </table>
    <hr>

    <a href="#modal-article" data-fancybox class="btn btn-primary">Asignar artículo</a>

    <div id="modal-article" style="width: 70%;display: none;">
    	<div class="container">
    		<div class="row">
    			<div class="col-lg-12">
    				<h3>Asignar artículo</h3>
    				<hr>
    				<table id="datatable" class="table table-striped table-bordered table-sm">
				      <thead class="thead-light">
				          <tr class="text-center">
				            <th>Nombre</th>
				            <th>Fecha</th>
				            <th>Acciones</th>
				          </tr>
				      </thead>
				      <tbody>
				      		@foreach ($posts_modal as $post)
					            <tr class="text-center">
					                <td>{{$post->CutTitle}}</td>
					                <td>{{$post->date}}</td>
					                <td>
					                	<ul>
					                		<li>
					                			<form action="{{route('clientspost.store')}}" method="post">
					                				@csrf
					                				<input type="hidden" value="{{$post->id}}" name="id">
					                				<input type="hidden" value="{{$user->id}}" name="client_id">
					                				<button type="submit" class="btn btn-success btn-xs">Asignar</button>
					                			</form>
					                		</li>
					                	</ul>
					                </td>
					            </tr>
				            @endforeach
				      </tbody>
				    </table>
    			</div>
    		</div>
    	</div>
    </div>

    <hr>
    <h2>Banners de publicidad</h2>

	<table class="datatable table table-striped table-sm banners">
      <thead class="thead-light">
          <tr class="text-center">
            <th>Nombre</th>
            <th>Cliente</th>
            <th>Fecha</th>
            <th>Acciones</th>
          </tr>
      </thead>
      <tbody>
      		@foreach ($banners as $banner)
	            <tr class="text-center">
	                <td>{{$banner->name}}</td>
	                <td>{{$banner->client->name}}</td>
	                <td>{{$banner->created_at->format('d-m-Y')}}</td>
	                <td>
	                	<ul>
	                		<li>
	                			<a href="{{route('banners.edit',$banner->id)}}" class="btn btn-success btn-xs">Editar</a>
	                		</li>
	                		@role('admin')
	                		<li>
	                			<form method="POST" action="{{ route('banners.destroy',$banner->id) }}">
								    @csrf
								    {{ method_field('delete') }}
								    <button type="submit" class="btn btn-danger btn-xs">Eliminar</button> 
								</form>	
	                		</li>
	                		@endrole
	                	</ul>
	                </td>
	            </tr>
            @endforeach
      </tbody>
    </table>
    <hr>

@endsection

@section('footer')
@endsection

@push('js')
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
	<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
	<script>
	 	datatables();
	 	datatabless();
	</script>
@endpush
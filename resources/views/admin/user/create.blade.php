@extends('layouts.admin')

@section('title','Agregar usuario')

@section('add-button')
	<a href="{{route('users.index')}}" class="btn btn-info">Regresar</a>
@endsection

@section('subtitle')
@endsection

@section('col','col-12')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Usuario</li>
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('assets/plugins/summernote/summernote-bs4.css')}}">
@endpush

@section('content')
	
	<form method="post" action="{{route('users.store')}}">
		@csrf
		<div class="form-group">
			<label>Nombre:</label>
			<input type="text" name="name" class="form-control" required>
		</div>
		<div class="form-group">
			<label>Correo electrónico:</label>
			<input type="email" name="email" class="form-control" required>
		</div>
		<div class="form-group">
			<label>Rol:</label>
			<select class="form-control" name="rol" required>
				<option value="admin">Administrador</option>
				<option value="editor">Editor</option>
				<option value="columnista">Columnista</option>
				<option value="cliente">Cliente</option>
			</select>
		</div>
		<div class="form-group">
			<label>Contraseña:</label>
			<input type="password" name="password" class="form-control" required>
		</div>
		<button type="submit" class="btn btn-success">Agregar usuario</button>
	</form>

@endsection

@section('footer')
@endsection

@push('js')
	<script src="{{asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
	<script src="{{asset('assets/plugins/summernote/summernote-bs4.min.js')}}"></script>
	<script>
		switch_init();
		summernote_init();
	</script>
@endpush
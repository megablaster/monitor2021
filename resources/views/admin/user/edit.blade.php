@extends('layouts.admin')

@section('title','Editar usuario')

@section('add-button')
	<a href="{{route('users.index')}}" class="btn btn-danger" style="margin-right: 5px;">Cancelar</a>
	<a href="#" id="save" class="btn btn-success">Guardar cambios</a>
@endsection

@section('subtitle')
@endsection

@section('col','col-12')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item active">Usuario</li>
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('assets/plugins/summernote/summernote-bs4.css')}}">
@endpush

@section('content')
	
	<form method="post" id="form-global" action="{{route('users.update',$user->id)}}"  enctype="multipart/form-data">
		@if ($errors->any())
	     	@foreach ($errors->all() as $error)
		     	<div class="alert alert-danger">
			        <ul>
			            <li>{{$error}}</li>
			        </ul>
			    </div>
	     	@endforeach
		@endif
		@if (Session::has('msg-success'))
		    <div class="alert alert-success">
		        <ul>
		            <li>{!! Session::get('msg-success') !!}</li>
		        </ul>
		    </div>
		@endif
		@csrf
		{{ method_field('PUT') }}
		<div class="form-group">
			<label>Nombre:</label>
			<input type="text" name="name" class="form-control" value="{{$user->name}}">
		</div>
		<div class="form-group">
			<label>Correo electrónico:</label>
			<input type="text" name="email" class="form-control" value="{{$user->email}}">
		</div>
		<hr>
		<div class="row">
			<div class="col-xl-12">
				<h5>Cambiar contraseña:</h5>
			</div>
			<div class="col-xl-6">
				<div class="form-group">
					<label>Contraseña:</label>
					<input type="password" name="password" class="form-control">
				</div>
			</div>
			<div class="col-xl-6">
				<div class="form-group">
					<label>Repetir contraseña:</label>
					<input type="password" name="password_confirmation" class="form-control">
				</div>
			</div>
		</div>
		<hr>
		@role('admin')
			<div class="form-group">
				<label>Roles:</label>
				<select class="form-control" name="rol" required>
					@php
						$rol = $user->roles->pluck('name');
					@endphp
					<option value="admin" @if ($rol[0] == 'admin') selected @endif>Administrador</option>
					<option value="editor" @if ($rol[0] == 'editor') selected @endif>Editor</option>
					<option value="columnista" @if ($rol[0] == 'columnista') selected @endif>Columnista</option>
					<option value="cliente" @if ($rol[0] == 'cliente') selected @endif>Cliente</option>
				</select>
			</div>
		@endrole
		<div class="form-group">
			<label>Biografía:</label>
			<textarea class="form-control summernote" name="bio">{!!$user->bio!!}</textarea>
		</div>
		<div class="form-group">
			<label>Imagen:</label>
			<input type="file" name="img" class="form-control"><hr>
			<img src="{{$user->img}}" class="img-fluid" style="max-width: 300px;">
		</div>
		<button type="submit" class="btn btn-success btn-hidden">Guardar</button>
	</form>

@endsection

@section('footer')
@endsection

@push('js')
	<script src="{{asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
	<script src="{{asset('assets/plugins/summernote/summernote-bs4.min.js')}}"></script>
	<script>
		summernote_init();
		switch_init();
	</script>
@endpush
@extends('layouts.admin')

@section('title',$post->name)

@section('col','col-12')

@section('subtitle')
	<strong>Fecha de creación:</strong> {{$post->created_at->format('d-m-Y')}}
@endsection

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Administrador</a></li>
  <li class="breadcrumb-item"><a href="{{route('posts.index')}}">Noticias</a></li>
  <li class="breadcrumb-item active">{{$post->name}}</li>
@endsection

@section('content')
	<p>
		<strong>Nombre:</strong> {{$post->name}}<br>
		<strong>Estatus:</strong> {{$post->status}}<br>
		<strong>Categoría:</strong> {{$post->name}}<br>
		<strong>Imagen:</strong> {{$post->img}}<br>
		<strong>Descripción:</strong> <br>{{$post->description}}
	</p>
@endsection

@section('footer')
	<a href="{{route('posts.index')}}" class="btn btn-info">Regresar</a>
@endsection

@push('js')
@endpush
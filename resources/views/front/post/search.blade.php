@extends('front.layout')

@section('title','Búsqueda')

@section('content')

	<div class="site-main-container">
		<!-- Start top-post Area -->
		<section class="top-post-area pt-10">
			<div class="container no-padding">
				<div class="row">
					<div class="col-lg-12">
						<div class="hero-nav-area">
							<h1 class="text-white">Búsqueda:</h1>
							<p class="text-white link-nav">
								<a href="{{ route('front.index') }}">Inicio </a> <span class="lnr lnr-arrow-right"></span>
								<a href="#">Búsqueda</a>
							</p>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="news-tracker-wrap">
		                    <h6><span>Últimas noticias:</span>
		                    	<a href="{{route('front.single',[$data['more']->category->url,$data['more']->url])}}">{{$data['more']->name}}</a>
		                    </h6>
		                </div>
					</div>
				</div>
			</div>
		</section>
		<!-- End top-post Area -->
		<!-- Start latest-post Area -->
		<section class="latest-post-area pb-120">
			<div class="container no-padding">
				<div class="row">
					<div class="col-lg-8 post-list">
						<!-- Start latest-post Area -->
						<div class="latest-post-wrap">
							<h4 class="cat-title">Resultado de búsqueda: <strong>{{$data['search']}}</strong></h4>

								@foreach ($posts as $post)

									<div class="single-latest-post row align-items-center">
										<div class="col-lg-5 post-left">
											<div class="feature-img relative">
												<div class="overlay overlay-bg"></div>
												<img class="img-fluid" src="{{$post->img}}" alt="{{$post->name}}">
											</div>
											<ul class="tags">
												<li><a href="{{route('front.single',[$post->category->url,$post->url])}}">{{$post->category->name}}</a></li>
											</ul>
										</div>
										<div class="col-lg-7 post-right">
											<a href="{{route('front.single',[$post->category->url,$post->url])}}">
												<h4>{{$post->name}}</h4>
											</a>
											<ul class="meta">
												<li><a href="#"><span class="lnr lnr-user"></span>{{$post->author->name ?? 'Anónimo' }}</a></li>
												<li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$post->date}}</a></li>
												<li><a href="#"><span class="lnr lnr-eye"></span>{{$post->view}}</a></li>
											</ul>
											<p class="excert">{!!$post->excerpt!!}</p>
										</div>
									</div>

								@endforeach
							
							<div class="load-more">
								 {{ $posts->links() }}
							</div>
							
						</div>
						<!-- End latest-post Area -->
					</div>
					<div class="col-lg-4">
						<div class="sidebars-area">
							<div class="single-sidebar-widget editors-pick-widget">
								<h6 class="title">Más noticias</h6>
								<div class="editors-pick-post">
									<div class="feature-img-wrap relative">
										<div class="feature-img relative">
											<div class="overlay overlay-bg"></div>
											<img class="img-fluid" src="{{$data['featured']->img}}" alt="{{$data['featured']->name}}">
										</div>
										<ul class="tags">
											<li><a href="{{route('front.single',[$data['featured']->category->url,$data['featured']->url])}}">{{$data['featured']->category->name}}</a></li>
										</ul>
									</div>
									<div class="details">
										<a href="{{route('front.single',[$data['featured']->category->url,$data['featured']->url])}}">
											<h4 class="mt-20">{{$data['featured']->name}}</h4>
										</a>
										<ul class="meta">
											<li><a href="{{route('front.single',[$data['featured']->category->url,$data['featured']->url])}}"><span class="lnr lnr-user"></span>{{$data['featured']->author->name ?? 'Anónimo'}}</a></li>
											<li><a href="{{route('front.single',[$data['featured']->category->url,$data['featured']->url])}}"><span class="lnr lnr-calendar-full"></span>{{$data['featured']->date}}</a></li>
										</ul>
										<p class="excert">{!!$data['featured']->excerpt!!}</p>
									</div>
									<div class="post-lists">

										@foreach ($data['others'] as $other)
									
											<div class="single-post d-flex flex-row">
												<div class="thumb" style="background-image: url('{{$other->img}}');min-width: 100px; height:80px;background-size: cover;background-position: center;">
												</div>
												<div class="detail">
													<a href="#"><h6>{{$other->name}}</h6></a>
													<ul class="meta">
														<li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$other->date}}</a></li>
														<li><a href="#"><span class="lnr lnr-bubble"></span>{{$other->view}}</a></li>
													</ul>
												</div>
											</div>

										@endforeach
									</div>
								</div>
							</div>
							<div class="single-sidebar-widget most-popular-widget">
								<h6 class="title">Destacadas de la semana</h6>

								@foreach ($data['moreView'] as $view)
									
									<div class="single-list flex-row d-flex">
										<div class="thumb" style="background-image: url('{{$view->img}}');min-width: 100px; height:80px;background-size: cover;background-position: center;">
										</div>
										<div class="details">
											<a href="{{route('front.single',[$view->category->url,$view->url])}}">
												<h6>{{$view->name}}</h6>
											</a>
											<ul class="meta">
												<li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$view->date}}</a></li>
												<li><a href="#"><span class="lnr lnr-bubble"></span>{{$view->view}}</a></li>
											</ul>
										</div>
									</div>

								@endforeach

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End latest-post Area -->
	</div>

@endsection
@extends('front.layout')

@section('title',$category->name)

@push('css')
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.theme.default.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
@endpush

@section('content')

	<div class="site-main-container">
		<!-- Start top-post Area -->
		<section class="top-post-area pt-10">
			<div class="container no-padding">
				<div class="row">
					<div class="col-lg-12">
						<div class="hero-nav-area">
							<h1 class="text-white">{{$user->name}}</h1>
							<p class="text-white link-nav">
								<a href="{{ route('front.index') }}">Inicio </a> <span class="lnr lnr-arrow-right"></span>
								<a href="{{ route('front.category',$category->url) }}">{{$category->name}}</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<br>

		<!-- End top-post Area -->
		<!-- Start latest-post Area -->
		<section class="latest-post-area pb-120" style="margin-top:0;">
			<div class="container no-padding">
				<div class="row">
					<div class="col-lg-8 post-list">
						<!-- Start latest-post Area -->
						<div class="latest-post-wrap">

							@if ($banners->count() > 0)
								
								 <div class="owl-carousel owl-theme home-carousel">
			                    	@foreach ($banners as $full)
			                        	<div class="item">
			                        		<a href="{{route('front.url',[$full->id,'home_full'],)}}" target="_blank">
			                        			<img src="{{$full->img_home_full}}" alt="{{$full->name}}" class="img-fluid" title="{{$full->name}}">
			                        		</a>
			                        	</div>
			                    	@endforeach
			                    </div>
			                    <br>

		                    @endif

							<h4 class="cat-title">Últimas noticias</h4>

								@foreach ($postFeatured as $post)

									<div class="single-latest-post row align-items-center">
										<div class="col-lg-5 post-left">
											<a href="{{route('front.single',[$post->category->url,$post->url])}}">
												<div class="feature-img relative">
													<div class="overlay overlay-bg"></div>
													<img class="img-fluid" src="{{$post->img}}" alt="{{$post->name}}">
												</div>
											</a>
											<ul class="tags">
												<li><a href="{{route('front.single',[$post->category->url,$post->url])}}">{{$post->author->name}}</a></li>
											</ul>
										</div>
										<div class="col-lg-7 post-right">
											<a href="{{route('front.single',[$post->category->url,$post->url])}}">
												<h4>{{$post->name}}</h4>
											</a>
											<ul class="meta">
												<li><a href="#"><span class="lnr lnr-user"></span>{{$post->author->name}}</a></li>
												<li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$post->date}}</a></li>
												<li><a href="#"><span class="lnr lnr-eye"></span>{{$post->view}}</a></li>
											</ul>
											<p class="excert">
												{{$post->excerpt}}
											</p>
										</div>
									</div>

								@endforeach

								@foreach ($postSecond as $post)

									<div class="single-latest-post row align-items-center">
										<div class="col-lg-5 post-left">
											<a href="{{route('front.single',[$post->category->url,$post->url])}}">
												<div class="feature-img relative">
													<div class="overlay overlay-bg"></div>
													<img class="img-fluid" src="{{$post->img}}" alt="{{$post->name}}">
												</div>
											</a>
											<ul class="tags">
												<li><a href="{{route('front.single',[$post->category->url,$post->url])}}">{{$post->author->name}}</a></li>
											</ul>
										</div>
										<div class="col-lg-7 post-right">
											<a href="{{route('front.single',[$post->category->url,$post->url])}}">
												<h4>{{$post->name}}</h4>
											</a>
											<ul class="meta">
												<li><a href="#"><span class="lnr lnr-user"></span>{{$post->author->name}}</a></li>
												<li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$post->date}}</a></li>
												<li><a href="#"><span class="lnr lnr-eye"></span>{{$post->view}}</a></li>
											</ul>
											<p class="excert">
												{{$post->excerpt}}
											</p>
										</div>
									</div>

								@endforeach
							
							<div class="load-more">
								<div class="text-center">
									{{$postSecond->links()}}
								</div>
							</div>
							
						</div>
						<!-- End latest-post Area -->
					</div>
					<div class="col-lg-4">
						<div class="sidebars-area">
							<div class="single-sidebar-widget editors-pick-widget">
								<h6 class="title">Más noticias</h6>
								<div class="editors-pick-post">

									<div class="feature-img-wrap relative">
										<div class="feature-img relative">
											<div class="overlay overlay-bg"></div>
											<img class="img-fluid" src="{{$featured->img}}" alt="{{$featured->name}}">
										</div>
										<ul class="tags">
											<li><a href="{{route('front.single',[$featured->category->url,$featured->url])}}">{{$featured->category->name}}</a></li>
										</ul>
									</div>
									<div class="details">
										<a href="{{route('front.single',[$featured->category->url,$featured->url])}}">
											<h4 class="mt-20">{{$featured->name}}</h4>
										</a>
										<ul class="meta">
											<li><a href="{{route('front.single',[$featured->category->url,$featured->url])}}"><span class="lnr lnr-user"></span>{{$featured->author->name}}</a></li>
											<li><a href="{{route('front.single',[$featured->category->url,$featured->url])}}"><span class="lnr lnr-calendar-full"></span>{{$featured->date}}</a></li>
										</ul>
										<p class="excert">{!!$featured->excerpt!!}</p>
									</div>

									<div class="post-lists">

										@foreach ($latest as $other)
									
											<div class="single-post d-flex flex-row">
												<div class="thumb" style="background-image: url('{{$other->img}}');min-width: 100px; height:80px;background-size: cover;background-position: center;">
												</div>
												<div class="detail">
													<a href="{{route('front.single',[$featured->category->url,$featured->url])}}"><h6>{{$other->name}}</h6></a>
													<ul class="meta">
														<li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$other->date}}</a></li>
														<li><a href="#"><span class="lnr lnr-bubble"></span>{{$other->view}}</a></li>
													</ul>
												</div>
											</div>

										@endforeach
										
									</div>

								</div>
							</div>
							<div class="single-sidebar-widget ads-widget">
								@if ($bannersRight->count() > 0)

				                	<div class="single-sidebar-widget ads-widget">
				                        
				                        <div class="owl-carousel owl-theme banner-right">
				                        	@foreach ($bannersRight as $right)
					                        	<div class="item">
					                        		<a href="{{route('front.url',[$right->id,'home_right'],)}}" target="_blank">
					                        			<img src="{{$right->img_home_left}}" alt="{{$right->name}}" class="img-fluid" title="{{$right->name}}">
					                        		</a>
					                        	</div>
				                        	@endforeach
				                        </div>

				                    </div>	                		

			                	@endif
							</div>
							<div class="single-sidebar-widget most-popular-widget">
								<h6 class="title">Destacadas de la semana</h6>

								@foreach ($moreView as $view)
							
									<div class="single-list flex-row d-flex">
										<div class="thumb" style="background-image: url('{{$view->img}}');min-width: 100px; height:80px;background-size: cover;background-position: center;">
										</div>
										<div class="details">
											<a href="{{route('front.single',[$view->category->url,$view->url])}}">
												<h6>{{$view->name}}</h6>
											</a>
											<ul class="meta">
												<li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$view->date}}</a></li>
												<li><a href="#"><span class="lnr lnr-bubble"></span>{{$view->view}}</a></li>
											</ul>
										</div>
									</div>

								@endforeach
								
							</div>

						</div>
					</div>
				</div>
				</div>
			</div>
		</section>
		<!-- End latest-post Area -->
	</div>

	@if (!is_null($bannersModal))
		
		<div id="modal" style="max-width: 700px;display: none;padding: 20px;">
			<div class="container">
				<div class="row">
					<div class="col-lg-12" style="padding:0;">
						<a href="{{route('front.url',[$bannersModal->id,'modal'])}}" target="_blank">
	            			<img src="{{$bannersModal->img_modal}}" alt="{{$bannersModal->name}}" class="img-fluid" title="{{$bannersModal->name}}">
	            		</a>
					</div>
				</div>
			</div>
		</div>

		<a href="#modal" id="click-modal" data-fancybox style="display: none;"></a>

	@endif
	

@endsection

@push('js')
	<script src="{{ asset('script/owl/owl.carousel.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
	@if (!is_null($bannersModal))
		<script>
			$("#click-modal").trigger('click');
		</script>
	@endif
	<script>
		$('.home-carousel').owlCarousel({
		    loop:true,
		    margin:0,
		    dots:false,
		    nav:false,
		    autoplay:true,
			autoplayTimeout:3500,
			autoplayHoverPause:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		});

		$('.banner-right').owlCarousel({
		    loop:true,
		    margin:0,
		    nav:false,
		    autoplay:true,
			autoplayTimeout:3500,
			autoplayHoverPause:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		});
	</script>
@endpush
@extends('front.layout')

@section('seo')
 {!! SEO::generate() !!}
 <meta name="twitter:card" content="summary_large_image">
 <meta name="twitter:image" content="{{$data['urlImage']}}">
@endsection

@section('title',$data['post']->name)

@push('css')
	<style>
		body {
			font-size: inherit!important;
		}
		.media {
		    display: inherit!important;
		    align-items: inherit!important;
		}
		img {
			width: 100%;
		}
	</style>
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.theme.default.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
@endpush

@section('content')

	<div id="fb-root"></div>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v8.0&appId=589746951167260&autoLogAppEvents=1" nonce="0wbuEtBQ"></script>

	<!-- Start latest-post Area -->
	<section class="latest-post-area pb-120">
		<div class="container no-padding">
			<div class="row">
				<div class="col-lg-8 post-list">
					<!-- Start single-post Area -->
					<div class="single-post-wrap">
						<div class="feature-img-thumb relative">
							<div class="overlay overlay-bg"></div>
							<img class="img-fluid" src="{{url($data['post']->img)}}" alt="{{$data['post']->name}}">
						</div>
						<div class="content-wrap">
							<ul class="tags">
								<li><a href="{{route('front.category',$data['post']->category->url)}}">{{$data['post']->category->name}}</a></li>
							</ul>
							<a href="#">
								<h3>{{$data['post']->name}}</h3>
							</a>
							<ul class="meta pb-20">
								<li><a href="#"><span class="lnr lnr-user"></span>{{$data['post']->Anonymo}}</a></li>
								<li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$data['post']->date}}</a></li>
							</ul>
							{!!$data['post']->description!!}

							@if ($data['post']->slides->count() > 0)
								<h3>Galería</h3><hr>

								<div class="owl-carousel owl-theme banner-right">

									@foreach ($data['post']->slides as $slide)
										<div class="item">
											<a href="{{$slide->img}}" data-fancybox="images" style="background-image: url('{{$slide->img}}');height: 200px;display: block;background-size:cover;">
											</a>
										</div>
									@endforeach

								</div>	
							@endif
							
							<div id="share"></div>

							@if ($data['post']->category->author == 'on')
								
								<div id="author" style="border: 1px solid #ececec;padding: 15px;margin-top: 15px;">
									<div class="container">
										<div class="row">
											<div class="col-lg-4">
												<div class="img" style="background-image: url('{{$data['post']->author->img}}');border:3px solid #ececec;width: 100%;height:150px;background-size: cover;background-position: center;bottom: -25px;position:relative;background-color: #ececec;"></div>
											</div>
											<div class="col-lg-8" style="font-size: 13px;line-height: 20px;">
												<h5>{{$data['post']->author->name}}</h5>
												{!!$data['post']->author->bio!!}
											</div>
										</div>
									</div>
								</div>

							@endif

							<div class="comment-sec-area">
								<div class="container">
									<div class="row flex-column">
										<h6>Comentarios</h6>
										<div class="fb-comments" data-href="{{Request::fullUrl()}}" data-numposts="5" data-width="100%"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End single-post Area -->
					<br>
					<?php if ($data['post']->banner): ?>
						<a href="{{route('front.url',[$data['post']->banner->id,'article'])}}" target="_blank">
							<img src="{{ $data['post']->banner->img_home_full }}" alt="{{$data['post']->banner->name}}" class="img-fluid" title="{{$data['post']->banner->name}}">
						</a>
					<?php endif ?>
					
			</div>
			<div class="col-lg-4">
				<div class="sidebars-area">
					<?php if ($data['post']->banner): ?>
						<a href="{{route('front.url',[$data['post']->banner->id,'article'])}}" target="_blank">
							<img src="{{ $data['post']->banner->img_article }}" alt="{{$data['post']->banner->name}}" class="img-fluid" title="{{$data['post']->banner->name}}">
						</a>
					<?php endif ?>
					<br><br>
					<div class="single-sidebar-widget editors-pick-widget">
						<h6 class="title">Más noticias</h6>
						<div class="editors-pick-post">

							<div class="feature-img-wrap relative">
								<a href="{{route('front.single',[$data['featured']->category->url,$data['featured']->url])}}">
									<div class="feature-img relative">
										<div class="overlay overlay-bg"></div>
										<img class="img-fluid" src="{{$data['featured']->img}}" alt="{{$data['featured']->name}}">
									</div>
								</a>
								<ul class="tags">
									<li><a href="{{route('front.single',[$data['featured']->category->url,$data['featured']->url])}}">{{$data['featured']->category->name}}</a></li>
								</ul>
							</div>
							<div class="details">
								<a href="{{route('front.single',[$data['featured']->category->url,$data['featured']->url])}}">
									<h4 class="mt-20">{{$data['featured']->name}}</h4>
								</a>
								<ul class="meta">
									<li><a href="{{route('front.single',[$data['featured']->category->url,$data['featured']->url])}}"><span class="lnr lnr-user"></span>{{$data['featured']->author->name ?? 'Anónimo' }}</a></li>
									<li><a href="{{route('front.single',[$data['featured']->category->url,$data['featured']->url])}}"><span class="lnr lnr-calendar-full"></span>{{$data['featured']->date}}</a></li>
								</ul>
								<p class="excert">{!!$data['featured']->excerpt!!}</p>
							</div>
							
							<div class="post-lists">

								@foreach ($data['posts'] as $other)
							
									<div class="single-post d-flex flex-row">
										<a href="{{route('front.single',[$other->category->url,$other->url])}}">
											<div class="thumb" style="background-image: url('{{$other->img}}');min-width: 100px; height:80px;background-size: cover;background-position: center;">
											</div>
										</a>
										<div class="detail">
											<a href="{{route('front.single',[$other->category->url,$other->url])}}"><h6>{{$other->name}}</h6></a>
											<ul class="meta">
												<li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$other->date}}</a></li>
											</ul>
										</div>
									</div>

								@endforeach
								
							</div>
								
						</div>
					</div>

					@if ($data['post']->banners->count() > 0)
						
						<div class="single-sidebar-widget ads-widget">

							<div class="owl-carousel owl-theme banner-right">
								@foreach ($data['post']->banners as $banners)
									<img class="img-fluid" src="{{$banners->img_article}}" alt="{{$banners->name}}" class="{{$banners->name}}">
								@endforeach
							</div>

						</div>

					@endif

					<div class="single-sidebar-widget most-popular-widget">
						<h6 class="title">Destacadas de la semana</h6>

						@foreach ($data['moreView'] as $view)
							
							<div class="single-list flex-row d-flex">
								<a href="{{route('front.single',[$view->category->url,$view->url])}}">
									<div class="thumb" style="background-image: url('{{$view->img}}');min-width: 100px; height:80px;background-size: cover;background-position: center;">
									</div>
								</a>
								<div class="details">
									<a href="{{route('front.single',[$view->category->url,$view->url])}}">
										<h6>{{$view->name}}</h6>
									</a>
									<ul class="meta">
										<li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$view->date}}</a></li>
									</ul>
								</div>
							</div>

						@endforeach

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('js')
	<script charset="utf-8" src="//cdn.iframe.ly/embed.js?api_key=25ea538cdb05e50a58e50c"></script>
	<script src="{{ asset('script/owl/owl.carousel.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
	<script>
		$('.banner-right').owlCarousel({
		    loop:true,
		    margin:0,
		    nav:false,
		    autoplay:true,
			autoplayTimeout:3500,
			autoplayHoverPause:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:2
		        },
		        1000:{
		            items:3
		        }
		    }
		});
	</script>
	<script>
		$(document).ready(function(){
			document.querySelectorAll( 'oembed[url]' ).forEach( element => {
		        iframely.load( element, element.attributes.url.value );
		    });
		});
	</script>
@endpush
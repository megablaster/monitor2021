@extends('front.layout')

@section('title')
	Bienvenidos a {{$data['systems']->name_company}}
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.theme.default.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.carousel.min.css')}}">
@endpush

@section('content')

	<!-- Start top-post Area -->
	<section class="top-post-area pt-10">
	    <div class="container no-padding">
	        <div class="row small-gutters">

		            <div class="col-lg-8 top-post-left">
		            	<div class="owl-carousel owl-theme home-carousel-featured">
		            		
		            		@foreach ($data['HomeNews']->take(6) as $home)
			            		<div class="item">

			            			<a href="{{route('front.single',[$home->category->url,$home->url])}}">
				            			<div class="feature-image-thumb relative">
						                    <div class="overlay overlay-bg"></div>
						                    <img class="img-fluid" src="{{url($home->img)}}" alt="{{$home->name}}">
						                </div>
					            	</a>

					                <div class="top-post-details">
					                    <ul class="tags">
					                        <li><a href="{{route('front.category',$home->category->url)}}">{{$home->category->name}}</a></li>
					                    </ul>
					                    <a href="{{route('front.single',[$home->category->url,$home->url])}}">
					                        <h3>{{$home->name}}</h3>
					                    </a>
					                    <ul class="meta">
					                        <li><a href="{{route('front.single',[$home->category->url,$home->url])}}"><span class="lnr lnr-user"></span>{{$home->author->name ?? 'Anónimo'}}</a></li>
					                        <li><a href="{{route('front.single',[$home->category->url,$home->url])}}"><span class="lnr lnr-calendar-full"></span>{{$home->date}}</a></li>
					                    </ul>
					                </div>
			            		</div>
		            		@endforeach
		            	</div>
		            </div>

		            <div class="col-lg-4 top-post-right">

		            	<div class="owl-carousel owl-theme home-carousel">

		            		@foreach ($data['HomeNews']->skip(6)->take(3) as $homeTwo)

			            		<div class="item">
					                <div class="single-top-post">
					                	<a href="{{route('front.single',[$homeTwo->category->url,$homeTwo->url])}}">
						                    <div class="feature-image-thumb relative">
						                        <div class="overlay overlay-bg"></div>
						                        <img class="img-fluid" src="{{url($homeTwo->img)}}" alt="">
						                    </div>
						                </a>
					                    <div class="top-post-details">
					                        <ul class="tags">
					                            <li><a href="{{route('front.category',$homeTwo->category->url)}}">{{$homeTwo->category->name}}</a></li>
					                        </ul>
					                        <a href="{{route('front.single',[$homeTwo->category->url,$homeTwo->url])}}">
					                            <h4>{{$homeTwo->name}}</h4>
					                        </a>
					                        <ul class="meta">
					                            <li><a href="#"><span class="lnr lnr-user"></span>{{$homeTwo->author->name ?? 'Anónimo' }}</a></li>
					                            <li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$homeTwo->date}}</a></li>
					                        </ul>
					                    </div>
					                </div>
				                </div>

			                @endforeach

			            </div>
			           
		                <div class="owl-carousel owl-theme home-carousel">

		                	@foreach ($data['HomeNews']->skip(9)->take(3) as $homeThree)

			            		<div class="item">

					                <div class="single-top-post mt-10">

					                	<a href="{{route('front.single',[$homeThree->category->url,$homeThree->url])}}">
						                    <div class="feature-image-thumb relative">
						                        <div class="overlay overlay-bg"></div>
						                        <img class="img-fluid" src="{{url($homeThree->img)}}" alt="">
						                    </div>
						                </a>
						                
					                    <div class="top-post-details">
					                        <ul class="tags">
					                            <li><a href="{{route('front.category',$homeThree->category->url)}}">{{$homeThree->category->name}}</a></li>
					                        </ul>
					                        <a href="{{route('front.single',[$homeThree->category->url,$homeThree->url])}}">
					                            <h4>{{$homeThree->name}}</h4>
					                        </a>
					                        <ul class="meta">
					                            <li><a href="#"><span class="lnr lnr-user"></span>{{$homeThree->author->name ?? 'Anónimo'}}</a></li>
					                            <li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$homeThree->date}}</a></li>
					                        </ul>
					                    </div>
					                </div>

					            </div>

				            @endforeach
				        </div>

	            	</div>

	            {{--<div class="col-lg-12">
	                <div class="news-tracker-wrap">
	                    <h6><span>Últimas noticias:</span>
	                    	<a href="{{route('front.single',[$data['more']->category->url,$data['more']->url])}}">{{$data['more']->name}}</a>
	                    </h6>
	                </div>
	            </div>--}}
	        </div>
	    </div>
	</section>
	<!-- End top-post Area -->
	
	<!-- Start latest-post Area -->
	<section class="latest-post-area pb-120">
	    <div class="container no-padding">
	        <div class="row">
	            <div class="col-lg-8 post-list">

					@if ($data['BannerHomepage']->count() > 0)
						<div class="col-lg-12 ad-widget-wrap mb-30">
				
							<div class="owl-carousel owl-theme banner-right">
								@foreach ($data['BannerHomepage'] as $full)
									<div class="item">
										<a href="{{route('front.url',[$full->id,'home_full'],)}}" target="_blank">
											<img src="{{$full->img_home_full}}" alt="{{$full->name}}" class="img-fluid" title="{{$full->name}}">
										</a>
									</div>
								@endforeach
							</div>
						
						</div>
	                @endif
					
	                <!-- Start latest-post Area -->
	                <div class="latest-post-wrap">
	                    <h4 class="cat-title">Últimas noticias</h4>

	                    @foreach ($data['latestNews'] as $new)

	                    	<div class="single-latest-post row align-items-center">
		                        <div class="col-lg-5 post-left">
		                        	<a href="{{route('front.single',[$new->category->url,$new->url])}}">
			                            <div class="feature-img relative">
			                                <div class="overlay overlay-bg"></div>
			                                <img class="img-fluid" src="{{url($new->img)}}" alt="{{$new->category->name}}">
			                            </div>
			                        </a>
		                            <ul class="tags">
		                                <li>
		                                	<a href="{{route('front.category',$new->category->url)}}">{{$new->category->name}}</a>
		                                </li>
		                            </ul>
		                        </div>
		                        <div class="col-lg-7 post-right">
		                            <a href="{{route('front.single',[$new->category->url,$new->url])}}">
		                                <h4>{{$new->name}}</h4>
		                            </a>
		                            <ul class="meta">
		                                <li><a href="{{route('front.single',[$new->category->url,$new->url])}}"><span class="lnr lnr-user"></span>{{ $new->author->name ?? 'Anónimo' }}</a></li>
		                                <li><a href="{{route('front.single',[$new->category->url,$new->url])}}"><span class="lnr lnr-calendar-full"></span>{{$new->date}}</a></li>
		                            </ul>
		                            <p class="excert">{!!$new->excerpt!!}</p>
		                        </div>
		                    </div>

	                    @endforeach
	                    
	                </div>
	                <!-- End latest-post Area -->
	                
	                <!-- Start banner-ads Area -->

	                @if ($data['BannerFull']->count() > 0)
	                	<div class="col-lg-12 ad-widget-wrap mt-30 mb-30">
                   
							<div class="owl-carousel owl-theme banner-right">
								@foreach ($data['BannerFull'] as $full)
									<div class="item">
										<a href="{{route('front.url',[$full->id,'home_full'],)}}" target="_blank">
											<img src="{{$full->img_home_full}}" alt="{{$full->name}}" class="img-fluid" title="{{$full->name}}">
										</a>
									</div>
								@endforeach
							</div>
		                   
		                </div>
		            @else
		            	<br>
	                @endif
	                <!-- End banner-ads Area -->

	                <!-- Start popular-post Area -->
	                @foreach ($data['categories']->where('featured','on')->where('author','on') as $key => $cat)

	                	<div class="popular-post-wrap">
		                    <h4 class="title">{{$cat->name}}</h4>

		                    <div class="row mt-20 medium-gutters">

		                    	@foreach ($cat->posts as $post)

		                    		<div class="col-lg-6 single-popular-post">
			                            <div class="feature-img-wrap relative">
			                            	@php
			                            		$slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $post->author->name)));
			                            	@endphp
			                            	<a href="{{route('front.category.user',[$post->category->url,$post->user_id,$slug ])}}">
				                                <div class="feature-img relative">
				                                    <div class="overlay overlay-bg"></div>
													@if(!is_null($post->author->img))
				                                    	<img class="img-fluid" src="{{url($post->author->img)}}" alt="{{$post->author->name ?? 'Anónimo' }}">
													@endif
				                                </div>
				                            </a>
			                                <ul class="tags">
			                                    <li><a href="{{route('front.category',[$new->category->url,$new->url])}}">{{$post->author->name}}</a></li>
			                                </ul>
			                            </div>
			                        </div>

		                    	@endforeach

		                    </div>
		                </div>

		                <!-- Start banner-ads Area -->
		                @if ($cat->banners->count() > 0)
		                	<div class="col-lg-12 ad-widget-wrap mt-30 mb-30">

								<div class="single-sidebar-widget most-popular-widget">	                        
			                        <div class="owl-carousel owl-theme banner-right">
			                        	@foreach ($data['BannerFull'] as $full)
				                        	<div class="item">
				                        		<a href="{{route('front.url',[$full->id,'home_full'],)}}" target="_blank">
				                        			<img src="{{url($full->img_home_full)}}" alt="{{$full->name}}" class="img-fluid" title="{{$full->name}}">
				                        		</a>
				                        	</div>
			                        	@endforeach
			                        </div>
			                    </div>

			                </div>
			            @else
			            	<br>
		                @endif
		                <!-- End banner-ads Area -->
	                @endforeach

	                <!-- Start popular-post Area -->
	                @foreach ($data['categories']->where('featured','on')->where('author','off') as $key => $cat)

	                	<div class="popular-post-wrap">
		                    <h4 class="title">{{$cat->name}}</h4>

		                    <div class="row mt-20 medium-gutters">

		                    	@foreach ($cat->posts as $post)

		                    		<div class="col-lg-6 single-popular-post">
			                            <div class="feature-img-wrap relative">
			                            	<a href="{{route('front.single',[$post->category->url,$post->url])}}">
				                                <div class="feature-img relative">
				                                    <div class="overlay overlay-bg"></div>
				                                    <img class="img-fluid" src="{{url($post->img)}}" alt="{{$post->name}}">
				                                </div>
				                            </a>
			                                <ul class="tags">
			                                    <li><a href="{{route('front.category',[$new->category->url,$new->url])}}">{{$post->category->name}}</a></li>
			                                </ul>
			                            </div>
			                            <div class="details">
			                                <a href="{{route('front.single',[$post->category->url,$post->url])}}">
			                                    <h4>{{$post->name}}</h4>
			                                </a>
			                                <ul class="meta">
			                                    <li><a href="{{route('front.single',[$new->category->url,$new->url])}}"><span class="lnr lnr-user"></span>{{$post->author->name}}</a></li>
			                                    <li><a href="{{route('front.single',[$new->category->url,$new->url])}}"><span class="lnr lnr-calendar-full"></span>{{$post->date}}</a></li>
			                                </ul>
			                                <p class="excert">
			                                    {!!$post->excerpt!!}
			                                </p>
			                            </div>
			                        </div>

		                    	@endforeach

		                    </div>
		                </div>
		                <!-- Start banner-ads Area -->
		                @if ($cat->banners->count() > 0)
		                	<div class="col-lg-12 ad-widget-wrap mt-30 mb-30">

								<div class="single-sidebar-widget most-popular-widget">	                        
			                        <div class="owl-carousel owl-theme banner-right">
			                        	@foreach ($cat->banners as $full)
											@if(!is_null($full->img_home_full))
												<div class="item">
													<a href="{{route('front.url',[$full->id,'home_full'],)}}" target="_blank">
														<img src="{{url($full->img_home_full)}}" alt="{{$full->name}}" class="img-fluid" title="{{$full->name}}">
													</a>
												</div>
											@endif
			                        	@endforeach
			                        </div>
			                    </div>

			                </div>
			            @else
			            	<br>
		                @endif
		                <!-- End banner-ads Area -->
	                @endforeach
	                
	                <!-- End popular-post Area -->
	            </div>
	            <div class="col-lg-4">
	                <div class="sidebars-area">

	                	@if ($data['BannerRight']->count() > 0)

							<div class="single-sidebar-widget most-popular-widget">
		                        
		                        <div class="owl-carousel owl-theme banner-right">
		                        	@foreach ($data['BannerRight'] as $right)

									    @if(!is_null($right->img_home_left))
											<div class="item">
												<a href="{{route('front.url',[$right->id,'home_right'],)}}" target="_blank">
													<img src="{{url($right->img_home_left)}}" alt="{{$right->name}}" class="img-fluid" title="{{$right->name}}">
												</a>
											</div>
										@endif

		                        	@endforeach
		                        </div>

		                    </div>	                		

	                	@endif
	                    
	                    <div class="single-sidebar-widget most-popular-widget">
	                        <h6 class="title">Noticias populares</h6>

	                        @foreach ($data['MoreNews'] as $more)

		                        <div class="single-list flex-row d-flex">
		                        	<a href="{{route('front.single',[$more->category->url,$more->url])}}">
			                        	<div class="thumb" style="background-image: url('{{url($more->img)}}');min-width: 100px; height:80px;background-size: cover;background-position: center;">
										</div>
									</a>
		                            <div class="details">
		                                <a href="{{route('front.single',[$more->category->url,$more->url])}}">
		                                    <h6>{{$more->name}}</h6>
		                                </a>
		                                <ul class="meta">
			                                <li><a href="#"><span class="lnr lnr-calendar-full"></span>{{$more->date}}</a></li>
		                                </ul>
		                            </div>
		                        </div>

	                        @endforeach

	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
	<!-- End latest-post Area -->
@endsection

@push('js')
	<script src="{{ asset('script/owl/owl.carousel.min.js') }}"></script>
	<script>
		$('.home-carousel').owlCarousel({
		    loop:true,
		    margin:0,
		    dots:false,
		    nav:false,
		    autoplay:true,
			autoplayTimeout:3500,
			autoplayHoverPause:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		});

		$('.home-carousel-featured').owlCarousel({
		    loop:true,
		    margin:0,
		    nav:false,
		    autoplay:true,
			autoplayTimeout:3500,
			autoplayHoverPause:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		});

		$('.banner-right').owlCarousel({
		    loop:true,
		    margin:0,
		    nav:false,
		    autoplay:true,
			autoplayTimeout:3500,
			autoplayHoverPause:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		});
	</script>
@endpush
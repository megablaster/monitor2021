<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="{{$data['systems']->favicon}}">
        <meta charset="UTF-8">
        <title>@yield('title') - {{$data['systems']->name_company}}</title>
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('assets/css/linearicons.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/nice-select.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/jquery-ui.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
        {!!$data['systems']->analytics!!}
        @yield('seo')
        @stack('css')
        <style>
            .tags li {
                background: {{$data['systems']->color_primary}}!important;
            }
            .main-menu {
                background: {{$data['systems']->color_primary}}!important;   
            }
            /*.header-top {
                background: {{$data['systems']->color_primary}}!important;      
            }*/
        </style>
    </head>
    <body>
        <header>
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-6 header-top-left no-padding">
                            <ul>
                                @if ($data['systems']->facebook)
                                    <li><a href="{{$data['systems']->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a></li>    
                                @endif
                                @if ($data['systems']->twitter)
                                    <li><a href="{{$data['systems']->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                @endif
                                @if ($data['systems']->instagram)
                                    <li><a href="{{$data['systems']->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                @endif
                                @if ($data['systems']->linkedin)
                                    <li><a href="{{$data['systems']->linkedin}}"><i class="fa fa-linkedin"></i></a></li>
                                @endif
                                @if ($data['systems']->whatsapp)
                                    <li><a href="{{$data['systems']->whatsapp}}"><i class="fa fa-whatsapp"></i></a></li>
                                @endif
                            </ul>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-6 header-top-right no-padding">
                            <ul>
                                <li><a href="tel:{{$data['systems']->phone}}"><span class="lnr lnr-phone-handset"></span><span>{{$data['systems']->phone}}</span></a></li>
                                <li><a href="mailto:{{$data['systems']->email}}"><span class="lnr lnr-envelope"></span><span>{{$data['systems']->email}}</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="logo-wrap">
                <div class="container">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-lg-3 col-md-3 col-sm-12 logo-left no-padding">
                            <a href="{{ route('front.index') }}">
                                <img class="img-fluid" src="{{$data['systems']->logo}}" alt="{{$data['systems']->name_company}}" title="{{$data['systems']->name_company}}">
                            </a>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 logo-right no-padding ads-banner">
                            <img class="img-fluid" src="img/banner-ad.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container main-menu" id="main-menu">
                <div class="row align-items-center justify-content-between">
                    <nav id="nav-menu-container">
                        <ul class="nav-menu">
                            @include('front.parts.navbar')
                        </ul>
                    </nav>
                    <div class="navbar-right">
                        <form class="Search" method="get" action="{{ route('front.search') }}">
                            <input type="text" class="form-control Search-box" name="search" id="Search-box" placeholder="Buscar..">
                            <label for="Search-box" class="Search-box-label">
                                <span class="lnr lnr-magnifier"></span>
                            </label>
                            <span class="Search-close">
                                <span class="lnr lnr-cross"></span>
                            </span>
                        </form>
                    </div>
                </div>
            </div>
        </header>
        
        <div class="site-main-container">
            @yield('content')
        </div>
        
        <!-- start footer Area -->
        <footer class="footer-area section-gap" style="padding:20px;">
            <div class="container">
                <div class="row" style="display: none;">
                    <div class="col-lg-3 col-md-6 single-footer-widget">
                        <h4>Categorías</h4>
                        <ul>
                            @foreach ($data['categories'] as $cat)
                                <li><a href="{{ route('front.category',$cat->url) }}">{{$cat->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-6 single-footer-widget">
                        <h4>Quick Links</h4>
                        <ul>
                            <li><a href="#">Jobs</a></li>
                            <li><a href="#">Brand Assets</a></li>
                            <li><a href="#">Investor Relations</a></li>
                            <li><a href="#">Terms of Service</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-6 single-footer-widget">
                        <h4>Features</h4>
                        <ul>
                            <li><a href="#">Jobs</a></li>
                            <li><a href="#">Brand Assets</a></li>
                            <li><a href="#">Investor Relations</a></li>
                            <li><a href="#">Terms of Service</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-6 single-footer-widget">
                        <h4>Resources</h4>
                        <ul>
                            <li><a href="#">Guides</a></li>
                            <li><a href="#">Research</a></li>
                            <li><a href="#">Experts</a></li>
                            <li><a href="#">Agencies</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 single-footer-widget">
                        <h4>Instragram Feed</h4>
                        <ul class="instafeed d-flex flex-wrap">
                            <li><img src="img/i1.jpg" alt=""></li>
                            <li><img src="img/i2.jpg" alt=""></li>
                            <li><img src="img/i3.jpg" alt=""></li>
                            <li><img src="img/i4.jpg" alt=""></li>
                            <li><img src="img/i5.jpg" alt=""></li>
                            <li><img src="img/i6.jpg" alt=""></li>
                            <li><img src="img/i7.jpg" alt=""></li>
                            <li><img src="img/i8.jpg" alt=""></li>
                        </ul>
                    </div>
                </div>
                <div class="footer-bottom row align-items-center" style="padding:20px;margin-top:0;">
                    <p class="footer-text m-0 col-lg-8 col-md-12">
                        Copyright &copy; {{ date('Y') }} {{$data['systems']->name}}. Todos los derechos reservados.</p>
                        <div class="col-lg-4 col-md-12 footer-social">
                            @if ($data['systems']->facebook)
                                <a href="{{$data['systems']->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a>    
                            @endif
                            @if ($data['systems']->twitter)
                                <a href="{{$data['systems']->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a>
                            @endif
                            @if ($data['systems']->instagram)
                                <a href="{{$data['systems']->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a>
                            @endif
                            @if ($data['systems']->linkedin)
                                <a href="{{$data['systems']->linkedin}}"><i class="fa fa-linkedin"></i></a>
                            @endif
                            @if ($data['systems']->whatsapp)
                                <a href="{{$data['systems']->whatsapp}}"><i class="fa fa-whatsapp"></i></a>
                            @endif
                        </div>
                </div>
            </div>
        </footer>
        <!-- End footer Area -->
        <script src="{{asset('assets/js/vendor/jquery-2.2.4.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="{{asset('assets/js/vendor/bootstrap.min.js')}}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
        <script src="{{asset('assets/js/easing.min.js')}}"></script>
        <script src="{{asset('assets/js/hoverIntent.js')}}"></script>
        <script src="{{asset('assets/js/superfish.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.ajaxchimp.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('assets/js/mn-accordion.js')}}"></script>
        <script src="{{asset('assets/js/jquery-ui.js')}}"></script>
        <script src="{{asset('assets/js/jquery.nice-select.min.js')}}"></script>
        <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('assets/js/mail-script.js')}}"></script>
        <script src="{{asset('assets/js/main.js')}}"></script>
        @stack('js')
    </body>
</html>
@extends('front.layout')

@section('title','Contacto')

@section('content')

	<div class="site-main-container">
		<!-- Start top-post Area -->
		<section class="top-post-area pt-10">
			<div class="container no-padding">
				<div class="row">
					<div class="col-lg-12">
						<div class="hero-nav-area">
							<h1 class="text-white">Contacto</h1>
							<p class="text-white link-nav"><a href="{{ route('front.index') }}">Inicio </a>  <span class="lnr lnr-arrow-right"></span>
								<a href="#">Contacto </a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End top-post Area -->
		<!-- Start contact-page Area -->
		<section class="contact-page-area pt-50 pb-120">
			<div class="container">
				<div class="row contact-wrap">
					<div class="col-xl-12">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3765.226911135465!2d-98.24403653459248!3d19.315957499404277!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfd93e12d9cdeb%3A0x7107539d6899263b!2sGrupo%20Monitor%20MX!5e0!3m2!1ses-419!2smx!4v1602014322899!5m2!1ses-419!2smx"width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
						<br><br>
					</div>
					<div class="col-lg-3 d-flex flex-column address-wrap">
						<div class="single-contact-address d-flex flex-row">
							<div class="icon">
								<span class="lnr lnr-home"></span>
							</div>
							<div class="contact-details">
								<h5>Dirección</h5>
								<p>{{$systems->direction}}</p>
							</div>
						</div>
						<div class="single-contact-address d-flex flex-row">
							<div class="icon">
								<span class="lnr lnr-phone-handset"></span>
							</div>
							<div class="contact-details">
								<h5>Teléfono</h5>
								<p>{{$systems->phone}}</p>
							</div>
						</div>
						<div class="single-contact-address d-flex flex-row">
							<div class="icon">
								<span class="lnr lnr-envelope"></span>
							</div>
							<div class="contact-details">
								<h5>Correo</h5>
								<p>{{$systems->email}}</p>
							</div>
						</div>
					</div>
					<div class="col-lg-9">
						@if (Session::has('msg-success'))
							<div class="alert alert-success">
								<ul>
									<li>{!! Session::get('msg-success') !!}</li>
								</ul>
							</div>
						@endif
						<form class="form-area contact-form text-right" action="{{route('front.save.contact')}}" method="post">
							@csrf
							<div class="row">
								<div class="col-lg-6">
									<input name="name" placeholder="Nombre" class="common-input mb-20 form-control" required type="text">
									<input name="phone" placeholder="Teléfono / WhatsApp" class="common-input mb-20 form-control" required="" type="phone">
									<input name="email" placeholder="Correo electrónico" class="common-input mb-20 form-control" required="" type="email">
									<input name="subject" placeholder="Asunto" class="common-input mb-20 form-control" required="" type="text">
								</div>
								<div class="col-lg-6">
									<textarea class="common-textarea form-control" name="message" placeholder="Mensaje" required></textarea>
								</div>
								<div class="col-lg-12">
									<div class="alert-msg" style="text-align: left;"></div>
									<button type="submit" class="primary-btn primary" style="float: right;">Enviar consulta</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
		<!-- End contact-page Area -->
	</div>

@endsection
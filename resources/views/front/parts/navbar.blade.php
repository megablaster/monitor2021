<li class="menu-active"><a href="{{route('front.index')}}">Inicio</a></li>
<li class="menu-has-children"><a href="#">Categorías</a>
    <ul>
        @foreach ($data['categories'] as $cat)
            @if($cat->name != 'Portada')
            <li><a href="{{ route('front.category',$cat->url) }}">{{$cat->name}}</a></li>
            @endif
        @endforeach
    </ul>
</li>
<li><a href="{{route('front.contact')}}">Contacto</a></li>
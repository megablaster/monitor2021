@component('mail::message')
<h2>Hola, Administrador</h2>
<p><strong>Se ha recibido una solicitud de contacto:</strong></p>
<p>
<strong>Fecha:</strong> {{$lead->created_at}}<br>
<strong>Nombre:</strong> {{$lead->name}}<br>
<strong>Correo electrónico:</strong> {{$lead->email}}<br>
<strong>Teléfono:</strong> {{$lead->phone}}<br>
<strong>Contacto:</strong> {{$lead->contact}}<br>
<strong>Comentarios:</strong><br> {{$lead->comments}}
</p>

@component('mail::button', ['url' => config('app.url').'/admin/leads'.$lead->id])
	Ver prospecto
@endcomponent

Sinceramente,<br>
{{ config('app.name') }}
@endcomponent




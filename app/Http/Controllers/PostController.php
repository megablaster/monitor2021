<?php

namespace App\Http\Controllers;

use Image;
use App\Post;
use App\User;
use App\Banner;
use App\Client;
use App\Category;
use App\ClientPost;
use App\Gallery;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use DataTables;
use Storage;
use Carbon\Carbon;

class PostController extends Controller
{
    public function index()
    {
        return view('admin.post.index');
    }

    public function getPosts(Request $request)
    {
        if ($request->ajax()) {

            if (auth()->user()->hasRole('admin')) {

                $data = Post::with([
                    'category' => function($query) {
                        $query->select('id','name','url');
                    }, 
                    'author' => function($query) {
                        $query->select('id','name');
                    }, 
                ])->orderBy('id','desc')->withTrashed()->get(['id','name','status','created_at','view','category_id','user_id','url','deleted_at']);

            } else {

                $data = Post::with([
                    'category' => function($query) {
                        $query->select('id','name','url');
                    }, 
                    'author' => function($query) {
                        $query->select('id','name');
                    }, 
                ])
                ->where('user_id',auth()->user()->id)
                ->orderBy('id','desc')
                ->get(['id','name','status','created_at','view','category_id','user_id','url']);

            }

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('cuttitle', function($post){
                    return $post->cuttitle;
                })
                ->addColumn('status', function($post){

                    if ($post->trashed()) {
                        return '<span class="badge badge-danger">Eliminado</span>';
                    } elseif($post->status == 'on') {
                        return '<span class="badge badge-success">Público</span>';
                    } else {
                        return '<span class="badge badge-warning">Sin publicar</span>';
                    }

                })
                ->addColumn('category', function($post){
                    return '<span class="badge badge-primary">'.$post->category->name.'</span>';
                })
                ->addColumn('date', function($post){
                    return $post->created_at->format('d-m-Y');
                })
                ->addColumn('action', function($post){

                    $today = Carbon::today();
                    $created = $post->created_at;

                    $hours = $created->diffInHours($today);

                    if(auth()->user()->hasRole('admin') && $post->trashed()){

                        $actionBtn = '
                        <a href="'.route('post.restore',$post->id).'" class="btn btn-warning btn-xs">Restaurar</a>
                        <a href="'.route('front.single',[$post->category->url,$post->url]).'" target="_blank" class="btn btn-info btn-xs">Ver</a>
                        <a href="'.route('posts.edit',$post->id).'" class="btn btn-success btn-xs">Editar</a>
                        <form method="post" action="'.route("posts.destroy",$post->id).'" style="display:inline-block;">
                            '.csrf_field().'
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="delete btn btn-danger btn-xs">Borrar</button>
                        </form>';

                    } elseif(auth()->user()->hasRole('admin')){

                        $actionBtn = '
                        <a href="'.route('front.single',[$post->category->url,$post->url]).'" target="_blank" class="btn btn-info btn-xs">Ver</a>
                        <a href="'.route('posts.edit',$post->id).'" class="btn btn-success btn-xs">Editar</a>
                        <form method="post" action="'.route("posts.destroy",$post->id).'" style="display:inline-block;">
                            '.csrf_field().'
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="delete btn btn-danger btn-xs">Borrar</button>
                        </form>';
                    
                    } elseif(auth()->user()->hasRole('editor') && $hours > 48){

                        $actionBtn = '
                            <a href="'.route('front.single',[$post->category->url,$post->url]).'" target="_blank" class="btn btn-info btn-xs">Ver</a>';

                    } else {

                        $actionBtn = '
                        <a href="'.route('front.single',[$post->category->url,$post->url]).'" target="_blank" class="btn btn-info btn-xs">Ver</a>
                        <a href="'.route('posts.edit',$post->id).'" class="btn btn-success btn-xs">Editar</a>';
                        
                    } 
                    
                    return $actionBtn;
                })
                ->rawColumns(['cuttitle','category','date','status','action'])
                ->make(true);
        }
    }

    public function create()
    {
        $data = [
            'categories' => Category::status()->get()
        ];
        
        return view('admin.post.create',compact('data'));
    }

    public function store(Request $request)
    {
        $post = new Post;
        $post->name = $request->name;
        $post->created_at = $request->created_at;
        $post->url = Str::of($request->name)->slug('-');
        $post->description = $request->description;
        $post->category_id = $request->category_id;
        $post->created_at = NOW();
        $post->user_id = $request->user_id;
        $post->save();

        return redirect()->route('posts.edit',$post->id);
    }

    public function show(Post $post)
    {   
        return view('admin.post.show',compact('post'));
    }

    public function edit(Post $post)
    {
        $data = [
            'categories' => Category::status()->get(),
            'post' => $post,
            'autors' => User::role(['editor','columnista','admin'])->get(),
            'galleries' => Gallery::where('post_id',$post->id)->get(),
            'banners' => Banner::where('status','on')->get(),
            'clients' => User::role('cliente')->get(),
            'clientPost' => ClientPost::where('post_id',$post->id)->first()
        ];
        return view('admin.post.edit',compact('data'));
    }

    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'name' => 'required',
        ]);

        // dd($request->all());

        if ($request->client_id == null) {

            $client = ClientPost::where('post_id',$post->id)->first();

            if ($client) {
                $client->delete();
            }

        } else {

            $client = ClientPost::where('post_id',$post->id)->first();

            if (!is_null($client)) {

                $client->client_id = $request->client_id;
                $client->save();

            } else {

                $client = new ClientPost;
                $client->post_id = $post->id;
                $client->client_id = $request->client_id;
                $client->save();

            }

        }

        //Guardamos
        $post->name = $request->name;
        $post->created_at = $request->created_at;
        $post->user_id = $request->autor_id;

        // dd($request->all());

        // dd($request->all());
        $post->status = $request->status == 'on'? 'on':'off';
        $post->featured = $request->featured == 'on'? 'on':'off';
        $post->anonymous = $request->anonymous == 'on'? 'on':'off';

        $post->url = Str::of($request->name)->slug('-');

        if (!is_null($request->banner_id)) {
            $post->banner_id = $request->banner_id;
        } else {
            $post->banner_id = Null;
        }

        $post->description = $request->description;
        $post->category_id = $request->category_id;
        $post->save();

        return redirect()->route('posts.index')->with('msg-success','Se ha actualizado la noticia satisfactoriamente.');
    }

    public function restore($id)
    {
        Post::withTrashed()->find($id)->restore();

        return redirect()->route('posts.index')->with('msg-success','Se ha restaurado la noticia.');

    }

    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->back()->with('msg-success','Se ha borrado el registro satisfactoriamente.');
    }

    public function upload_image(Request $request)
    {
        //Obtenemos el post
        $post = Post::find($request->id);

        $this->validate($request, [
            'img' => 'image|mimes:jpeg,png,jpg,gif|max:6048',
        ]);

        //If img
        if ($request->img) {

            //Remove image
            Storage::delete($post->img);

            //Upload image
            $imageName = time().'.'.$request->img->extension();  
            $request->img->move(public_path('files/post'), $imageName);
            $post->img = '/files/post/'.$imageName;
        
            $post->save();

            return redirect()->back()->with('msg-success','Se ha subido la portada satisfactoriamente.');
        }
    }
}

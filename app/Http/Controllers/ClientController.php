<?php

namespace App\Http\Controllers;

use App\User;
use App\Post;
use App\Banner;
use App\ClientPost;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data = [
            'users' => User::role('cliente')->get(),
        ];

        return view('admin.client.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $posts = ClientPost::where('client_id',$id)->get();
        $banners = Banner::where('user_id',$id)->get();
        $posts_modal = Post::get();

        return view('admin.client.edit',compact('user','posts','posts_modal','banners'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->except('_token','_method'));

        // Le asignamos el rol de Cliente
        if ($request->rol == 'editor') {
            $user->syncRoles('editor');
        } elseif($request->rol == 'columnista') {
            $user->syncRoles('columnista');
        } elseif($request->rol == 'cliente') {
            $user->syncRoles('cliente');
        } else {
            $user->syncRoles('admin');
        }

        return redirect()->back()->with('msg-success','Se actualizo la información.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('clients.index')->with('msg-success','Se ha borrado el usuario correctamente.');
    }
}

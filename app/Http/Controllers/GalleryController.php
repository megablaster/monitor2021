<?php

namespace App\Http\Controllers;

use Image;
use Storage;
use App\Gallery;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload_gallery(Request $request)
    {
        $this->validate($request, [
            'gallery' => 'required',
            'gallery.*' => 'image|mimes:jpeg,png,jpg|max:6048',
        ]);

        //If img
        if ($request->gallery) {

            foreach ($request->file('gallery') as $file) {

                //Remove image
                Storage::delete($file);

                //Upload image
                $imageName = time().'.'.$file->extension();  
                $file->move(public_path('files/gallery'), $imageName);

                //Update img
                $gallery = new Gallery;
                $gallery->img = '/files/gallery/'.$imageName;
                $gallery->post_id = $request->id;
                $gallery->save();

            }
        }

        return redirect()->back()->with('msg-success','Se ha subido la galería satisfactoriamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $gallery = Gallery::where('id',$request->id)->first();

        //Remove image
        $image_path = public_path($gallery->img);
        if(File::exists($image_path)) {
            File::delete($image_path);
        }

        $gallery->delete();
        return redirect()->back()->with('msg-success','Se ha borrado la foto satisfactoriamente.');
    }
}

<?php

namespace App\Http\Controllers;
use App\Mail\SendLead;
use App\Post;
use App\User;
use App\Banner;
use App\Lead;
use App\System;
use App\Category;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class FrontController extends Controller
{
    public function index()
    {
        //More
        $More30 = Post::where('created_at', '>', Carbon::now()->subDays(30))->take(1)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','user_id','img','created_at']);

        if($More30 == null) {
            $More = Post::take(1)->with([
                'category' => function($query) {
                    $query->select('id','url', 'name');
                },
                'author' => function($query) {
                    $query->select('id','name');
                },
            ])->get(['id', 'name','url','category_id','user_id','img','created_at']);
        } else {
            $More = $More30;
        }

        //LatestNews
        $latestNews = Post::orderBy('created_at', 'desc')->limit(3)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','user_id','img','created_at','description']);

        //HomeNews
        $HomeNews = Post::orderBy('created_at', 'desc')->limit(12)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','user_id','img','created_at']);

        //MoreNews
        $MoreNews = Post::orderBy('created_at', 'desc')->limit(4)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','user_id','img','created_at']);

    	$data = [
            'systems' => System::find(1),
            'categories' => Category::status()->get(),
            'more' => $More,
            'latestNews' => $latestNews,
            'HomeNews' => $HomeNews,
            'MoreNews' => $MoreNews,
            'BannerRight' => Banner::where('status','on')->where('status_home_left','on')->get(),
            'BannerFull' => Banner::where('status','on')->where('status_home_full','on')->get(),
            'BannerHomepage' => Banner::where('status','on')->where('status_home_full','on')->where('category_id', 19)->get(),
    	];

    	return view('front.index',compact('data'));
    }

    public function single($cat,$name)
    {
        //Post
        $post = Post::where('url',$name)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->first();

        //Featured
        $featured = Post::inRandomOrder()->where('id','<>',$post->id)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->first(['id', 'name','url','category_id','user_id','img','created_at']);

        //Posts
        $posts = Post::inRandomOrder()->limit(3)->with([
            'category' => function($query) {
                $query->select('id','url','name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id','name','url','category_id','user_id','img','created_at']);

        //moreView
        $moreView = Post::moreview()->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','user_id','img','created_at']);

        //more
        $more = Post::where('created_at', '>', Carbon::now()->subDays(30))->take(1)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->first(['id', 'name','url','category_id','user_id','img','created_at']);

        //$post = Post::where('url',$name)->select('*')->with('category','author')->first();
        //$featured = Post::inRandomOrder()->where('id','<>',$post->id)->select('*')->with('category','author')->first();
        $fullImage = \URL::to('/').$post->img;

        $data = [
            'systems' => System::find(1),
            'categories' => Category::status()->get(),
            'more' => $more,
            'post' => $post,
            'urlImage' => $fullImage,
            'featured' => $featured,
            'posts' => $posts,
            'moreView' => $moreView,
            //'more' => Post::where('created_at', '>', Carbon::now()->subDays(30))->take(1)->select('*')->with('category','author')->first(),
            //'posts' => Post::inRandomOrder()->limit(3)->where('id','<>',$post->id)->where('id','<>',$featured->id)->select('*')->with('category','author')->get(),
            //'moreView' => Post::moreview()->select('*')->with('category','author')->get()
        ];

        $data['post']->view = $post->view + 1;
        $data['post']->save();

        //SEO
        SEOTools::setTitle($data['post']->name);
        SEOTools::setDescription($data['post']->excerpt);
        SEOTools::opengraph()->setUrl(url()->current());
        SEOTools::setCanonical(url()->current());
        SEOTools::twitter()->setSite($data['systems']->twitter_name);
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addImage($fullImage);
        SEOTools::jsonLd()->addImage($fullImage);

        return view('front.post.single',compact('data'));
    }

    public function category($cat)
    {
        $category = Category::where('url',$cat)->first(['id','url','name']);

        $postFeatured = Post::where('category_id',$category->id)->orderBy('created_at', 'desc')->where('featured','on')->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','user_id','img','created_at','description']);

        $postSecond = Post::where('category_id',$category->id)->orderBy('created_at', 'desc')->where('featured','off')->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->paginate(8, ['id', 'name','url','category_id','user_id','img','created_at','description']);

        $more30 = Post::where('category_id',$category->id)->where('created_at', '>', Carbon::now()->subDays(30))->take(1)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->first(['id', 'name','url','category_id','user_id','img','created_at','description']);

        if($more30 == null) {

            $more = Post::where('category_id',$category->id)->take(1)->with([
                'category' => function($query) {
                    $query->select('id','url', 'name');
                },
                'author' => function($query) {
                    $query->select('id','name');
                },
            ])->first(['id', 'name','url','category_id','user_id','img','created_at','description']);
            
        } else {
            $more = $more30;
        }

        $featured30 = Post::where('category_id',$category->id)->where('created_at', '>', Carbon::now()->subDays(30))->take(1)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->first(['id', 'name','url','category_id','user_id','img','created_at','description']);

        if($featured30 == null) {

            $featured = Post::where('category_id',$category->id)->take(1)->with([
                'category' => function($query) {
                    $query->select('id','url', 'name');
                },
                'author' => function($query) {
                    $query->select('id','name');
                },
            ])->first(['id', 'name','url','category_id','user_id','img','created_at','description']);
            
        } else {

            $featured = $featured30;
        }

        $latest = Post::inRandomOrder()->where('category_id',$category->id)->limit(3)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','user_id','img','created_at']);

        $moreView = Post::moreview()->orderBy('created_at', 'desc')->where('category_id',$category->id)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','user_id','img','created_at']);

        $data = [
            'systems' => System::find(1),
            'categories' => Category::status()->get(),
            'category' => $category,
            'postFeatured' => $postFeatured,
            'postSecond' => $postSecond,
            'more' => $more,
            'featured' => $featured,
            'latest' => $latest,
            'moreView' => $moreView,
            'banners' => Banner::where('status','on')->where('status_home_full','on')->where('category_id',$category->id)->inRandomOrder()->limit(3)->get(),
            'bannersRight' => Banner::where('status','on')->where('status_home_left','on')->where('category_id',$category->id)->inRandomOrder()->limit(3)->get(),
            'bannersModal' => Banner::where('status','on')->where('status_modal','on')->where('category_id',$category->id)->inRandomOrder()->limit(1)->first(),
        ];

        return view('front.post.category',compact('data'));
    }

     public function categoryUser($cat,$user)
    {

        //Obtenemos la categoría
        $category = Category::where('url',$cat)->first();
        $user = User::find($user);

        //Obtenemos destacados y resto
        $postFeatured = Post::where('featured','on')->orderBy('created_at', 'desc')->where('category_id',$category->id)->where('user_id',$user->id)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','user_id','img','created_at']);

        $postSecond = Post::where('featured','off')->orderBy('created_at', 'desc')->where('category_id',$category->id)->where('user_id',$user->id)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->paginate(8,['id', 'name','url','category_id','user_id','img','created_at']);

        $featured = Post::inRandomOrder()->where('category_id',$category->id)->limit(3)->orderBy('created_at', 'desc')->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->first(['id', 'name','url','category_id','user_id','img','created_at']);

        $latest = Post::inRandomOrder()->where('category_id',$category->id)->limit(3)->orderBy('created_at', 'desc')->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','user_id','img','created_at']);

        $moreView = Post::moreview()->where('category_id',$category->id)->orderBy('created_at', 'desc')->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','user_id','img','created_at']);

        $data = [
            'systems' => System::find(1),
            'categories' => Category::status()->get(),
        ];


        //$postFeatured = Post::where('category_id',$category->id)->where('featured','on')->orderBy('created_at', 'desc')->where('user_id',$user->id)->get();
        //$postSecond = Post::where('category_id',$category->id)->where('featured','off')->orderBy('created_at', 'desc')->where('user_id',$user->id)->paginate(8);

        $banners = Banner::where('status','on')->where('status_home_full','on')->where('category_id',$category->id)->inRandomOrder()->limit(3)->get();
        $bannersRight = Banner::where('status','on')->where('status_home_left','on')->where('category_id',$category->id)->inRandomOrder()->limit(3)->get();
        $bannersModal = Banner::where('status','on')->where('status_modal','on')->where('category_id',$category->id)->inRandomOrder()->limit(1)->first();

        //Noticias destacadas
        //$featured = Post::inRandomOrder()->where('category_id',$category->id)->orderBy('created_at', 'desc')->first();
        //$latest = Post::inRandomOrder()->where('category_id',$category->id)->limit(3)->orderBy('created_at', 'desc')->get();
        //$moreView = Post::moreview()->where('category_id',$category->id)->orderBy('created_at', 'desc')->get();

        return view('front.post.category_user',compact('data','category','postFeatured','postSecond','latest','featured','moreView','banners','bannersRight','bannersModal','user'));
    }

    public function search(Request $request)
    {
        //Featured
        $featured = Post::inRandomOrder()->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->first(['id', 'name','url','category_id','user_id','img','created_at','description']);

        //More
        $more = Post::where('created_at', '>', Carbon::now()->subDays(30))->take(1)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->first(['id', 'name','url','category_id','user_id','img','created_at','description']);

        //Others
        $others = Post::inRandomOrder()->limit(3)->where('id','<>',$featured->id)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','img','user_id','created_at','description']);

        //MoreView
        $moreView = Post::moreview()->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id','user_id','img','created_at','description']);

        //Posts
        $posts = Post::where('name','LIKE', "%{$request->search}%")->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->paginate(8,['id', 'name','url','category_id','user_id','img','created_at','description']);

        $data = [
            'systems' => System::find(1),
            'categories' => Category::status()->get(),
            'more' => $more,
            'search' => $request->search,
            'featured' => $featured,
            'others' => $others,
            'moreView' => $moreView,
            //'moreView' => Post::moreview()->get()
            //'others' => Post::inRandomOrder()->limit(3)->where('id','<>',$featured->id)->get(),
            //'more' => Post::where('created_at', '>', Carbon::now()->subDays(30))->take(1)->select('*')->with('category','author')->first(),
            
        ];

        //$posts = Post::where('name','LIKE', "%{$request->search}%")->paginate(8);
        $posts->appends(['search' => $request->search]); 

        return view('front.post.search',compact('data','posts'));
    }

    public function contact()
    {
        $data = [
            'systems' => System::find(1),
            'categories' => Category::status()->get(),
        ];
        return view('front.contact',compact('data'));
    }

    public function save_contact(Request $request)
    {
        $lead = new Lead;
        $lead->name = $request->name;
        $lead->email = $request->email;
        $lead->phone = $request->phone;
        $lead->contact = $request->subject;
        $lead->comments = $request->message;
        $lead->save();

        return redirect()->back()->with('msg-success','Se ha enviado la consulta correctamente.');
    }

    public function url($id,$position)
    {
        $banner = Banner::find($id);

        if ($position == 'home_right') {
            $banner->click_home_left = $banner->click_home_left + 1;
        } elseif($position == 'home_full'){
            $banner->click_home_full = $banner->click_home_full + 1;
        } elseif($position == 'article') {
            $banner->click_article = $banner->click_article + 1;
        } elseif($position == 'modal') {
            $banner->click_modal = $banner->click_modal + 1;
        }

        $banner->save();

        return Redirect::to($banner->url);
    }
}

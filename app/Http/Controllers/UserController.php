<?php

namespace App\Http\Controllers;
use Image;
use Storage;
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use role;

class UserController extends Controller
{
    public function index()
    {
        $users = User::role(['admin','editor','columnista'])->get();
        return view('admin.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        // Le asignamos el rol de Cliente
        if ($request->rol == 'editor') {
            $user->syncRoles('editor');
        } elseif($request->rol == 'columnista') {
            $user->syncRoles('columnista');
        } elseif($request->rol == 'cliente') {
            $user->syncRoles('cliente');
        } else {
            $user->syncRoles('admin');
        }

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->bio = $request->bio;

        if ($request->password && $request->password_confirmation) {

            $request->validate([
              'password' => 'required|string|min:6|confirmed',
              'password_confirmation' => 'required',
            ]);

            $user->password = bcrypt($request->password);
        }

        $user->save();

        // Le asignamos el rol de Cliente
        if ($request->rol == 'editor') {
            $user->syncRoles('editor');
        } elseif($request->rol == 'columnista') {
            $user->syncRoles('columnista');
        } elseif($request->rol == 'cliente') {
            $user->syncRoles('cliente');
        } else {
            $user->syncRoles('admin');
        }

           
        //If img
        if ($request->img) {

            $this->validate($request, [
                'img' => 'image|mimes:jpeg,png,jpg|max:6048',
            ]);

            //Remove image
            $image_path = public_path($user->img);
            if(File::exists($image_path)) {
                File::delete($image_path);
            }

            $file = $request->file('img');
            $path = $file->hashName('public/files/user');
            $image = Image::make($file)->resize(null, 600, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put($path, (string) $image->encode());
            $url = Storage::url($path);    

            //Update img
            $user->img = $url;
            $user->save();

            return redirect()->back()->with('msg-success','Se ha subido la imagen correctamente.');
        }

        return redirect()->back()->with('msg-success','Se actualiza los datos de usuario.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->posts->count() >= 1) {
            return redirect()->back()->with('msg-error','No se puede borrar el usario, ya que cuenta con noticias.');
        } else {
            $user->delete();
            return redirect()->back()->with('msg-success','Se ha borrado el usuario satisfactoriamente.');
        }
    }

    public function upload_image(Request $request)
    {
        //Obtenemos el post
        $post = Post::find($request->id);

        $this->validate($request, [
            'img' => 'image|mimes:jpeg,png,jpg|max:6048',
        ]);

        //If img
        if ($request->img) {

            //Remove image
            $image_path = public_path($post->img);
            if(File::exists($image_path)) {
                File::delete($image_path);
            }

            $file = $request->file('img');
            $path = $file->hashName('public/files/post');
            $image = Image::make($file)->resize(null, 600, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put($path, (string) $image->encode());
            $url = Storage::url($path);    

            //Update img
            $post->img = $url;
            $post->save();

            return redirect()->back()->with('msg-success','Se ha subido la portada satisfactoriamente.');
        }
    }
}

<?php

namespace App\Http\Controllers;

use Image;
use App\Post;
use App\Category;
use App\System;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class SystemController extends Controller
{
    public function index()
    {
        $system = System::find('1');
        return view('admin.system.edit',compact('system'));
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
    }

    public function show(Post $post)
    {   
    }


    public function edit(Post $post)
    {
        $data = [  
            'categories' => Category::status()->get(),
            'post' => $post,
        ];
        return view('admin.post.edit',compact('data'));
    }

    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'logotipo' => 'image|mimes:jpeg,png,jpg,svg',
            'favicon' => 'image|mimes:jpeg,png,jpg',
        ]);

         //Guardamos
        $system = System::find(1);
        $system->name_company = $request->name_company;
        $system->direction = $request->direction;
        $system->phone = $request->phone;
        $system->email = $request->email;
        $system->website = $request->website;
        $system->facebook = $request->facebook;
        $system->twitter = $request->twitter;
        $system->linkedin = $request->linkedin;
        $system->instagram = $request->instagram;
        $system->whatsapp = $request->whatsapp;
        $system->analytics = $request->analytics;
        $system->twitter_name = $request->twitter_name;

        $system->color_primary = $request->color_primary;
        $system->color_secondary = $request->color_secondary;
        $system->color_bar = $request->color_bar;

        $system->rfc = $request->rfc;
        $system->user_facebook = $request->user_facebook;

        //Update img
        if ($request->logotipo) {
            $name = time().'_'.$request->logotipo->getClientOriginalName();
            $fileName = $name;
            $filePath = $request->file('logotipo')->storeAs('files/system', $fileName, 'public');
            $system->logo = '/storage/' . $filePath;
        }

        if ($request->favicon) {
            $name = time().'_'.$request->favicon->getClientOriginalName();
            $fileName = $name;
            $filePath = $request->file('favicon')->storeAs('files/system', $fileName, 'public');
            $system->favicon = '/storage/' . $filePath;
        }

        $system->save();

        return redirect()->route('systems.index')->with('msg-success','Se ha actualizado la configuración.');
    }

    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->back()->with('msg-success','Se ha borrado el registro satisfactoriamente.');
    }

    public function upload_image($request,$name)
    {
        $this->validate($request, [
            'img' => 'image|required|mimes:jpeg,png,jpg',
        ]);

        $originalImage= $request->file('img');
        $thumbnailImage = Image::make($originalImage);

        //Path
        $thumbnailPath = public_path().'/files/'.$name.'/thumbnail/';
        $originalPath = public_path().'/files/'.$name.'/';

        $thumbnailImage->save($originalPath.time().'-'.Str::of($request->name)->slug('-').'.'.$originalImage->getClientOriginalExtension());
        $thumbnailImage->resize(150,150);
        $thumbnailImage->save($thumbnailPath.time().'-'.Str::of($request->name)->slug('-').'.'.$originalImage->getClientOriginalExtension());

        return $img = time().'-'.Str::of($request->name)->slug('-').'.'.$originalImage->getClientOriginalExtension();
    }
}

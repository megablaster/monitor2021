<?php

namespace App\Http\Controllers;

use App\ClientPost;
use Illuminate\Http\Request;

class ClientPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $client = new ClientPost;
        $client->post_id = $request->id;
        $client->client_id = $request->client_id;
        $client->save();

        return redirect()->back()->with('msg-success','Se asigna artículo a cliente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClientPost  $clientPost
     * @return \Illuminate\Http\Response
     */
    public function show(ClientPost $clientPost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClientPost  $clientPost
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientPost $clientPost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClientPost  $clientPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientPost $clientPost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClientPost  $clientPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,ClientPost $clientPost)
    {
        $clientPost = clientPost::find($request->id);
        $clientPost->delete();
        return redirect()->back()->with('msg-success','Se ha borrado el artticulo asignado satisfactoriamente.');
    }
}

<?php

namespace App\Http\Controllers;

use Image;
use Storage;
use App\Banner;
use App\User;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'banners' => Banner::get()
        ];

        return view('admin.banner.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'clients' => User::role('cliente')->get(),
            'categories' => Category::get(),
        ];

        return view('admin.banner.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banner = Banner::create($request->except('_token'));
        return redirect()->route('banners.edit',$banner->id)->with('msg-success','Se creo la publicidad.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        $data = [
            'banner' => $banner,
            'categories' => Category::get()
        ];

        return view('admin.banner.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        if (is_null($request->status)) {
            $request->merge(['status'=>'off']);
        }

        if (is_null($request->status_home_left)) {
            $request->merge(['status_home_left'=>'off']);
        }

        if (is_null($request->status_modal)) {
            $request->merge(['status_modal'=>'off']);
        }

        if (is_null($request->status_home_full)) {
            $request->merge(['status_home_full'=>'off']);
        }

        if (is_null($request->status_article)) {
            $request->merge(['status_article'=>'off']);
        }

        $banner->update($request->all());

        return redirect()->route('banners.edit',[$banner->id])->with('msg-success','Se actualizo correctamente la publicidad.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        $img_home_left = public_path($banner->img_home_left);
        $img_home_full = public_path($banner->img_home_full);
        $img_article = public_path($banner->img_article);
        $img_modal = public_path($banner->img_modal);

        if(File::exists($img_home_left)) {
            File::delete($img_home_left);
        }
        if(File::exists($img_home_full)) {
            File::delete($img_home_full);
        }
        if(File::exists($img_article)) {
            File::delete($img_article);
        }
        if(File::exists($img_modal)) {
            File::delete($img_modal);
        }

        $banner->delete();
        return redirect()->back()->with('msg-success','Se ha borrado la cuenta publicitaria correctamente.');
    }

    //Imágenes
    public function lateral(Request $request)
    {
        $banner = Banner::find($request->id);

        $this->validate($request, [
            'img' => 'image|mimes:jpeg,png,jpg,gif|max:6048',
        ]);

        //If img
        if ($request->img) {

            //Remove image
            Storage::delete($banner->img_home_left);

            //Upload image
            $imageName = time().'.'.$request->img->extension();  
            $request->img->move(public_path('files/banner'), $imageName);
            $banner->img_home_left = '/files/banner/'.$imageName;
        
            $banner->save();

            return redirect()->back()->with('msg-success','Se ha subido la portada lateral satisfactoriamente.');
        }
    }

    public function full(Request $request)
    {
        $banner = Banner::find($request->id);

        $this->validate($request, [
            'img' => 'image|mimes:jpeg,png,jpg,gif|max:6048',
        ]);

        //If img
        if ($request->img) {

            //Remove image
            Storage::delete($banner->img_home_full);

            //Upload image
            $imageName = time().'.'.$request->img->extension();  
            $request->img->move(public_path('files/banner'), $imageName);
            $banner->img_home_full = '/files/banner/'.$imageName;
        
            $banner->save();

            return redirect()->back()->with('msg-success','Se ha subido la portada horizontal satisfactoriamente.');
        }
    }

    public function modal(Request $request)
    {
        $banner = Banner::find($request->id);

        $this->validate($request, [
            'img' => 'image|mimes:jpeg,png,jpg,gif|max:6048',
        ]);

        //If img
        if ($request->img) {

            //Remove image
            Storage::delete($banner->img_modal);

            //Upload image
            $imageName = time().'.'.$request->img->extension();  
            $request->img->move(public_path('files/banner'), $imageName);
            $banner->img_modal = '/files/banner/'.$imageName;
        
            $banner->save();

            return redirect()->back()->with('msg-success','Se ha subido el modal satisfactoriamente.');
        }
    }

    public function detail(Request $request)
    {
        $banner = Banner::find($request->id);

        $this->validate($request, [
            'img' => 'image|mimes:jpeg,png,jpg,gif|max:6048',
        ]);

        //If img
        if ($request->img) {

            //Remove image
            Storage::delete($banner->img_article);

            //Upload image
            $imageName = time().'.'.$request->img->extension();  
            $request->img->move(public_path('files/banner'), $imageName);
            $banner->img_article = '/files/banner/'.$imageName;
        
            $banner->save();

            return redirect()->back()->with('msg-success','Se ha subido la portada del artículo satisfactoriamente.');
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Category;
use App\Lead;
use App\Banner;

class AdminController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    public function displayImage($filename){

        $path = storage_path('public/' . $filename);

        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }
    
    public function index()
    {
        //LatestNews
        $latestNews = Post::orderBy('created_at', 'desc')->limit(3)->with([
            'category' => function($query) {
                $query->select('id','url', 'name');
            },
            'author' => function($query) {
                $query->select('id','name');
            },
        ])->get(['id', 'name','url','category_id']);

    	$data = [
    		'posts' => Post::get('id'),
    		'users' => User::get('id'),
    		'categories' => Category::get('id'),
    		'leads' => Lead::get('id'),
    		'moreView'=> Post::whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->orderBy('view','DESC')->first('name','view'),
    		'minusView'=> Post::whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->orderBy('view','ASC')->first('name','view'),
    		//'bannerMoreView'=> Banner::orderBy('click_home_left','DESC')->first(),
    		//'opinions' => Category::where('author','on')->first(),
    		'clients' => User::role('cliente')->get('id'),
    	];

    	return view('admin.index',compact('data'));
    }
}


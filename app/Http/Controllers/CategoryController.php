<?php

namespace App\Http\Controllers;

use Image;
use Storage;
use App\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('id', 'DESC')->get();
        return view('admin.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Save category
        $category = new Category();
        $category->name = $request->name;
        $category->url = Str::of($request->name)->slug('-');
        $category->save();

        return redirect()->route('categories.index')->with('msg-success','Se ha creado la categoría satisfactoriamente.');
    }

    public function show(Category $category)
    {
        //Change status
        $category->status = 'viewed';
        $category->save();

        return view('admin.category.show',compact('category'));
    }

    public function edit(Category $category)
    {
        return view('admin.category.edit',compact('category'));
    }

    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'img' => 'image|mimes:jpeg,png,jpg|max:6048',
        ]);

        //If img
        if ($request->img) {

            //Remove image
            $image_path = public_path($category->img);
            if(File::exists($image_path)) {
                File::delete($image_path);
            }

            $file = $request->file('img');
            $path = $file->hashName('public/files/category');
            $image = Image::make($file)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put($path, (string) $image->encode());
            $url = Storage::url($path);    

            //Update img
            $category->img = $url;
        }

        //Guardamos
        $category->name = $request->name;
        $category->status = $request->status == 'on'? 'on':'off';
        $category->featured = $request->featured == 'on'? 'on':'off';
        $category->author = $request->author == 'on'? 'on':'off';
        $category->url = Str::of($request->name)->slug('-');
        $category->save();

        return redirect()->route('categories.index')->with('msg-success','Se ha actualizado la categoría satisfactoriamente.');
    }

    public function destroy(Category $category)
    {
        if ($category->posts->count() >= 1) {
            return redirect()->back()->with('msg-error','No se puede borrar la categoría, ya que cuenta con noticias.');
        } else {
            $category->delete();
            return redirect()->back()->with('msg-success','Se ha borrado la registro satisfactoriamente.');
        }
    }
}

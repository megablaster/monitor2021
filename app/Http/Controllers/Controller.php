<?php

namespace App\Http\Controllers;
use App\Lead;
use View;
use App\System;
use App\Post;
use App\Category;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Carbon\Carbon;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $systems,$more_global,$categories;

    public function __construct() 
    {

    }

}

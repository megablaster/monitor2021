<?php

namespace App;
use App\Category;
use App\Gallery;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

	public function getExcerptAttribute()
	{
        $clean = strip_tags($this->description);
		return substr($clean,0,170).'...';
	}

    public function getCutTitleAttribute()
    {
        $title = substr($this->name,0,50).'...';
        return mb_convert_encoding($title, 'UTF-8', 'UTF-8');
    }

    public function getAnonymoAttribute()
    {
        if ($this->anonymous == 'on') {
            return 'Anónimo';
        } elseif(!empty($this->author->name)) {
            return $this->author->name;
        } else {
            return 'Anónimo';
        }
    }

    public function getImgAttribute($img)
    {
        if ($img == 'default.png' OR empty($img)) {
            return asset('default.jpeg');
        } else {
            return $img;
        }
    }

    public function getStatuAttribute()
    {
        if ($this->status == 'on') {
            return '<span class="badge badge-success">Público</span>';
        } else {
            return '<span class="badge badge-danger">Sin publicar</span>';
        }
    }

    public function getViewAttribute($count)
    {
        if ($count == 0) {
            return '0';
        } else {
            return $count;
        }
    }

    public function getDateAttribute()
    {
        setlocale(LC_TIME, 'es_ES');
        Carbon::setLocale('es');
        $date = Carbon::parse($this->created_at);
        return $date->formatLocalized('%d %B %Y');
    }

    public function scopeMore($query)
    {
        $now = Carbon::now();
        $weekStartDate = $now->startOfWeek()->format('Y-m-d H:i');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d H:i');

        return $this->where('view','>','5')->limit(1);
    }

    public function scopeOrder($query)
    {
        return $this->orderBy('created_at','desc');
    }

     public function scopeFeatured($query)
    {
        return $this->where('featured','on');
    }

    public function scopeMoreView($query)
    {
        $now = Carbon::now();
        $weekStartDate = $now->startOfWeek()->format('Y-m-d H:i');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d H:i');

        return $this->where('view','>','3')->orderBy('view','desc')->limit(4);
    }

    //Relation ships
    public function category()
    {
    	return $this->hasOne('App\Category','id','category_id');
    }

    public function author()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public function slides()
    {
        return $this->hasMany('App\Gallery','post_id','id');
    }

    public function banner()
    {
        return $this->hasOne('App\Banner','id','banner_id')->where('status','on')->where('status_article','on');
    }

    public function bannerFull()
    {
        return $this->hasOne('App\Banner','id','banner_id')->where('status','on')->where('status_home_full','on');
    }

    public function banners()
    {
        return $this->hasMany('App\Banner','id','banner_id')->where('status','on')->where('status_home_left','on')->inRandomOrder()->limit(5);
    }
}

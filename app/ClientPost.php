<?php

namespace App;
use App\Post;
use Illuminate\Database\Eloquent\Model;

class ClientPost extends Model
{
    protected $table = "client_post";

    public function post()
    {
    	return $this->hasOne(Post::class, 'id', 'post_id');
    }
}

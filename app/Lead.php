<?php

namespace App;
use App\Scopes\ActiveScope;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
	 protected $fillable = ['name','email','phone','contact','comments'];


	//Scopes
	public function scopeRead($query)
	{
		return $query->where('status','read');
	}

	//Mutators
	public function getViewedAttribute()
	{
		if ($this->status == 'read') {
			return '<span class="badge badge-pill badge-danger">Sin leer</span>';
		} else {
			return '<span class="badge badge-pill badge-success">Leído</span>';
		}
	}
}

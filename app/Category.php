<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	//Scopes
	public function scopeStatus($query)
	{
		return $this->where('status','on');
	}

	public function scopeFeatured($query)
	{
		return $this->where('featured','on');
	}

	public function getStatuAttribute()
    {
        if ($this->status == 'on') {
            return 'Público';
        } else {
            return 'Sin publicar';
        }
    }

	//Relationship
    public function posts()
    {
    	return $this->hasMany('App\Post')->limit(6)->orderBy('created_at','desc');
    }

    //Relationship
    public function banners()
    {
    	return $this->hasMany('App\Banner','category_id','id')->where('status','on')->where('status_home_full','on');
    }

}
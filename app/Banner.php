<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
	protected $guarded = [];

	public function getImgAttribute($img)
    {
        if ($img == 'default.png' OR $img == '') {
            return asset('default.jpeg');
        } else {
            return $img;
        }
    }

	//Relationship
    public function client()
    {
    	return $this->hasOne('App\User','id','user_id');
    }
}

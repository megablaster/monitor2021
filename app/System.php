<?php

namespace App;
use App\Category;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    public function getImgAttribute($img)
    {
        if ($img == 'default.png' OR $img == '') {
            return asset('default.jpeg');
        } else {
            return $img;
        }
    }

    public function getUrlPathAttribute()
    {
        return \Storage::url($this->logotipo);
    }

    //Relation ships
    public function category()
    {
    	return $this->hasOne('App\Category','id','category_id');
    }

    public function author()
    {
        return $this->hasOne('App\User','id','user_id');
    }
}

<?php

namespace App\Providers;
use App\Lead;
use App\Category;
use App\Post;
use App\System;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Auth;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider                                         
{
    public function register()
    {
        //
    }

    public function boot()
    {
      Schema::defaultStringLength(191);

      Carbon::setUTF8(true);
      Carbon::setLocale(config('app.locale'));
      setlocale(LC_TIME, config('app.locale'));

      //System
      if (Schema::hasTable('systems')) {
         $this->systems = System::find('1');
         View::share('systems', $this->systems);
      }

      //Leads
      if (Schema::hasTable('leads')) {
         $this->site_leads = Lead::read()->get();
         View::share('leads_global', $this->site_leads);
      }

       
       //More GLobal
        // $this->more = Post::where('created_at', '>', Carbon::now()->subDays(30))->take(1)->first();       
        // View::share('more_global', $this->more);

        //Categories

        // $this->categories = Category::status()->get();
        // View::share('categories_global', $this->categories);
    }
}

<?php

namespace App;
use App\ClientPost;
use App\Post;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function posts()
    {
    	return $this->hasMany(Post::class, 'id', 'post_id');
    }
}

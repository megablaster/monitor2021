<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banners', function($table){
            
            $table->set('status_modal',['on','off'])->default('off');
            $table->string('img_modal')->default('default.png');
            $table->integer('click_modal')->default('0');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('users', function($table){

            $table->dropColumn(['status_modal','img_modal','click_modal']);

        });
    }
}

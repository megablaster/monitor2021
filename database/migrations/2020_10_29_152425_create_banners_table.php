<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->set('status',['on','off'])->default('off');
            $table->set('status_home_left',['on','off'])->default('off');
            $table->set('status_home_full',['on','off'])->default('off');
            $table->set('status_article',['on','off'])->default('off');

            //Category and url
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');

            //Clients
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            //Url
            $table->string('url')->default('http://google.com');

            //Upload Image
            $table->string('img_home_left')->default('default.png');
            $table->string('img_home_full')->default('default.png');
            $table->string('img_article')->default('default.png');

            //Visit click
            $table->integer('click_home_left')->default('0');
            $table->integer('click_home_full')->default('0');
            $table->integer('click_article')->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}

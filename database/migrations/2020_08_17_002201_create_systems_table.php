<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('systems', function (Blueprint $table) {
            $table->id();
            $table->string('name_company')->default('Dashboard');
            $table->string('logo')->default('default.png');
            $table->string('favicon')->default('default.png');
            $table->string('color_primary')->default('#ececec');
            $table->string('color_secondary')->default('#ececec');            
            $table->string('color_bar')->default('#ececec');
            $table->string('direction')->default('Street 4')->nullable();
            $table->string('phone')->default('123456')->nullable();
            $table->string('email')->default('name@example.com')->nullable();
            $table->string('facebook')->default('facebook.com')->nullable();
            $table->string('twitter')->default('twitter.com')->nullable();
            $table->string('linkedin')->default('linkedin.com')->nullable();
            $table->string('instagram')->default('instagram.com')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('website')->nullable();
            $table->string('rfc', 100)->nullable();
            $table->string('user_facebook')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('systems');
    }
}

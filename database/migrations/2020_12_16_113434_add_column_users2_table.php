<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUsers2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table){
            $table->text('bio')->nullable();
            $table->string('img')->nullable();
        });

        Schema::table('categories', function($table){
            $table->set('author',['on','off'])->default('off');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table){
            $table->dropColumn(['bio','img']);
        });

        Schema::table('categories', function($table){
            $table->dropColumn(['author']);
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPostsMoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function($table){

            $table->unsignedBigInteger('banner_id')->nullable()->unsigned()->default(NULL);
            $table->foreign('banner_id')->references('id')->on('banners')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function($table){

            $table->dropForeign(['banner_id']);
            $table->dropColumn('banner_id');

        });
    }
}

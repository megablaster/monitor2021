<?php

use App\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        //inicializamos Faker en modo Español
        $category = Category::create(array(
            'name' => 'Opinión',
            'img' => 'default.png',            
            'status' => 'on',
            'url' => 'columnas'
        ));
    }
}

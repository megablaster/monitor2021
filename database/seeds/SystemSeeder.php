<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('systems')->insert([
            'name_company' => 'Monitor Xpress',
            'logo' => 'default.png',
            'favicon' => 'default.png',
            'color_primary' => '#ececec',
            'color_secondary' => '#ececec',
            'email' => 'contacto@monitorxpress.com',
            'phone' => '246 468 1506',
            'facebook' => 'https://www.facebook.com/MonitorXpress',
            'twitter' => 'https://twitter.com/monitorxpress',
            'linkedin' => '',
            'instagram' => 'https://www.instagram.com/monitorxpress',
            'direction' => 'Calle Allende 61-Altos, 90000 Tlaxcala de Xicohténcatl, México',
            'website' => 'https://www.monitorxpress.com',
            'created_at' => now(),
        ]);
    }
}

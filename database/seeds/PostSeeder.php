<?php

use App\Post;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	//inicializamos Faker en modo Español
        $faker = Faker\Factory::create('es_ES');

        for($a = 0; $a < 20; $a++) {
            //creamos el user guardando el nombre de la imágen.
            $post = Post::create(array(
	            'name' => $faker->sentence(3),
	            'status' => 'on',
	            'url' => Str::of($faker->word)->slug('-'),
	            'view' => $faker->biasedNumberBetween(1,100),
	            'img' => 'default.png',
	            'user_id' => 1,
	            'category_id' => $faker->biasedNumberBetween(1,4),
	            'description' => $faker->text,
            ));
        }

    }
}

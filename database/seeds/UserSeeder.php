<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;
use App\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrador',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'Redacción',
            'email' => 'redaccion@monitorxpress.com',
            'password' => Hash::make('Red@ccion2020'),
        ]);

        //Creamos los roles
        $admin = Role::create(['name' => 'admin']);
        $editor = Role::create(['name' => 'editor']);
        $columnista = Role::create(['name' => 'columnista']);
        $clientes = Role::create(['name' => 'cliente']);
        
        //Asignamos admin
        $user = User::find(1);
        $user->assignRole('admin');

        //Asignamos editor
        $editor = User::find(2);
        $editor->assignRole('editor');
    }
}

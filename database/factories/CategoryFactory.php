<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'img' => 'default.png',
        'status' => 'on',
        'url' => Str::of($faker->name)->slug('-')
    ];
});

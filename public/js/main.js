function summernote_init(){
	$('.summernote').summernote({
		height: 300,
		codemirror: { // codemirror options
		    theme: 'monokai'
		 },
		toolbar: [
		    ['style', ['bold', 'italic', 'underline', 'clear']],
		    ['font', ['strikethrough', 'superscript', 'subscript']],
		    ['fontsize', ['fontsize']],
		    ['color', ['color']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['height', ['height']],
		    ['insert',['link','video','picture']],
		    ['view', ['fullscreen', 'codeview', 'help']],
		]
	});
};

//Summernote
function datatabless(){
    $('.datatable').DataTable({
   	  "order": [[ 1, "asc" ]],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        }
    });
};


function datatables(){
    $('#datatable').DataTable({
   	  "order": [[ 1, "asc" ]],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        }
    });
};

function owl(){
	$('#carousel').owlCarousel({
	    loop:false,
	    margin:15,
	    nav:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:4
	        }
	    }
	});
}

//Switch
function switch_init(){
	$("input[data-bootstrap-switch]").each(function(){
	  $(this).bootstrapSwitch('state', $(this).prop('checked'));
	});	
};

$(document).ready(function(){

	//Save controller
	$('#save').on('click', function(e){
		e.preventDefault();
		jQuery('#form-global').submit();
	});
	
});